<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="{metadescription}" />
<meta name="keywords" content="{metakeywords}" />
<meta name="author" content="{author}" />
<title> {blogtitle} | {subtitle} </title>
<style type="text/css">
<!--
@import url("themes/default/zitemplate00.css");
-->
</style>
</head>

<body>
<div id="container">
<!-- header -->
<div id="header">
{header}
</div>
<!-- main menu -->
<div id="mainmenu">
<ul>
{mainlinks}
</ul>
</div>
<!-- content area -->
<div id="content">
<table>
<tr>
<td width="200" valign="top">
{add-ons}
{categories}
{archives}
{miscellaneous}
{adminmenu}
{usermenu}
</td>
<td width="560" valign="top">