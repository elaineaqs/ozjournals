<?php

# Chooses whether or not an image or text should be used for the header
if(strtolower($titleimg) == "none") {
 $blogheader .=  "<a href=\"index.php\"><h1> $blogtitle </h1></a><p class=\"slogan\"> $tagline </p>\n";
}
else {
 $blogheader .=  "<a href=\"index.php\"><img src=\"$titleimg\" border=\"0\" alt=\"$blogtitle\"></a>\n";
}

# Shows some main links
if($lstatus == "on") {
 $mainlinks .= "<li><a href=\"index.php\">$lang[37]</a></li> \n";

 # Shows pages
 if($pagestatus == "on") {
  $dir = "pages/";
  if(is_dir($dir)) {
   if($dh = opendir($dir)) {
    while(($file = readdir($dh)) !== false) {
     if($file !== "." AND $file !== "..") {
      if(filetype($dir.$file) !== "dir") {
       $pc = file_get_contents($dir.$file);
       $eachpc = explode("\t", $pc);
       $mainlinks .= "<li><a href=\"index.php?show=pages&f=".substr($file, 0, 1)."\">".$eachpc[1]."</a></li> \n";
      }
     }
    }
    closedir($dh);
   }
  }
  $ttp = countfiles("pages/");
  if($ttp == 0) {
   echo " ";
  }
 }
 $mainlinks .= "<li><a href=\"index.php?show=archives&p=main\">$lang[38]</a></li> \n";
 session_start();
 if($_SESSION['user'] == $user) {
  $mainlinks .= "<li><a href=\"admin.php\">$lang[31]</a></li> \n";
  $mainlinks .= "<li><a href=\"index.php?show=signout\">$lang[39]</a></li> \n";
 }
 elseif(strpos(file_get_contents("usersdb.php"), $_SESSION["user"]) !== FALSE) {
  $mainlinks .= "<li><a href=\"admin.php\">$lang[35]</a></li> \n"; 
  $mainlinks .= "<li><a href=\"index.php?show=signout\">$lang[39]</a></li> \n";
 }
 else {
  $mainlinks .= "<li><a href=\"index.php?show=signin\">$lang[2]</a></li> \n";
 }
}

# Shows the categories
if($catestatus == "on") {
 $categories .= "<h4> $lang[41] </h4>\n";
 if(file_exists("categories.php")) {
  $categoryfile = file_get_contents("categories.php");
  $eachcategory = explode("\n", $categoryfile);
  $allcategories = count($eachcategory) - 1;
  if($categoryfile == "") {
   $categories .= "$lang[42]<br />\n";
  }
  else {
   $categories .= "<ul>";
   $categories .= "<li><a href=\"index.php?show=archives&c=No-Category\">$lang[43]</a></li>\n";
   for($c = 0; $c < $allcategories; $c++) {
    $eachc = explode("\n", $categoryfile);
    $eachrealc = explode("\t", $eachc[$c]);	
    $categories .= "<li><a href=\"index.php?show=archives&c=".$eachrealc[1]."\">".str_replace("-", " ", $eachrealc[1])."</a></li>\n";
   }
   $categories .= "</ul>";
  }
 }
 else {
  $categories .= "$lang[44]<br />\n";
 }
}

# Shows the archives by month
if($archstatus == "on") {
 $archives .= "<h4> $lang[45] </h4>\n";
 if(countfiles ("archives") <= 0) {
  $archives .= "$lang[46]<br />\n";
 }
 $dir = "archives/";
 if(is_dir($dir)) {
  if($dh = opendir($dir)) {
   $archives .= "<ul>";
   while(($file = readdir($dh)) !== false) {
    if(filetype($dir . $file) == "file") {
     if($file !== "." AND $file !== "..") {
      $dfile = explode("-", $file);
      $month = str_replace(".php", "", $dfile[1]);
      $year = $dfile[0];
      $archives .= "<li><a href=\"index.php?show=archives&y=".$year."&m=".$month."\">$month $year</a></li>\n"; 
     }
    }
   }
   $archives .= "</ul>";
   closedir($dh);
  }
 }
}

# Shows miscellaneous links
if($miscstatus == "on") {
 $miscellaneous .= "<h4> $lang[47] </h4>\n";
 $miscellaneous .= "<ul>\n";
 $miscellaneous .= "<li><a href=\"index.php?show=rss\">$lang[91]</a></li>\n";
 $miscellaneous .= "<li><a href=\"javascript: window.print();\">$lang[48]</a></li>\n";
 $miscellaneous .= "<li><a href=\"javascript: window.external.AddFavorite(location.href,document.title);\">$lang[49]</a></li>\n";
 $miscellaneous .= "</ul>";
}

# Shows the add-ons if mode is not admin or user
if($_SESSION['mode'] == "admin" || $_SESSION['mode'] == "user") {
}
else {
if($astatus == "on") {
 if(file_exists("ads.php")) {
  $ads=file_get_contents("ads.php");
  if($ads !== "") {
   $addons .= stripslashes($ads)."<br />\n";
  }
  else {
   $addons .= "\n";
  }
 }
 else {
  $addons .= "$lang[40]<br />\n";
 }
}
}

# Shows Admin Menu
if($_SESSION['mode'] == "admin") {
 $adminmenu .= "<h4> Write </h4>\n";
 $adminmenu .= "<ul>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=add\">Journals</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=addpages\">Pages</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=addcategories\">Categories</a></li>\n";
 $adminmenu .= "</ul>";
 $adminmenu .= "<h4> Change </h4>\n";
 $adminmenu .= "<ul>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=editconfig\">Blog Settings</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=themes\">Themes</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=userconfig\">User Accounts</a></li>\n";
 $adminmenu .= "</ul>";
 $adminmenu .= "<h4> Edit/ Delete </h4>\n";
 $adminmenu .= "<ul>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=editjournals\">Journals</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=editpages\">Pages</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=editcategories\">Categories</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=editdeletecomments\">Comments</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=editarchives\">Archives</a></li>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=editads\">Add-ons</a></li>\n";
 $adminmenu .= "</ul>";
 $adminmenu .= "<h4> View </h4>\n";
 $adminmenu .= "<ul>\n";
 $adminmenu .= "<li><a href=\"admin.php?show=statistics\">Statistics</a></li>\n";
 $adminmenu .= "</ul>";
 $adminmenu .= "<h4> OZJournals </h4>\n";
 $adminmenu .= "<ul>\n";
 $adminmenu .= "<li><a href=\"readme.php\">Readme</a></li>\n";
 $adminmenu .= "<li><a href=\"license.txt\">License</a></li>\n";
 $adminmenu .= "<li><a href=\"https://sites.google.com/site/onlinezonejournals\" target=\"_blank\">Software Updates</a></li>\n";
 $adminmenu .= "</ul>";
}

# Shows User Menu
if($_SESSION['mode'] == "user") {
 $usermenu .= "<h4> Write </h4>\n";
 $usermenu .= "<ul>\n";
 $usermenu .= "<li><a href=\"admin.php?show=add\">Journals</a></li>\n";
 $usermenu .= "</ul>";
 $usermenu .= "<h4> Edit/ Delete </h4>\n";
 $usermenu .= "<ul>\n";
 $usermenu .= "<li><a href=\"admin.php?show=editjournals\">Journals</a></li>\n";
 $usermenu .= "</ul>";
 $usermenu .= "<h4> View </h4>\n";
 $usermenu .= "<ul>\n";
 $usermenu .= "<li><a href=\"admin.php?show=statistics\">Statistics</a></li>\n";
 $usermenu .= "</ul>";
 $usermenu .= "<h4> OZJournals </h4>\n";
 $usermenu .= "<ul>\n";
 $usermenu .= "<li><a href=\"readme.php\">Readme</a></li>\n";
 $usermenu .= "<li><a href=\"license.txt\">License</a></li>\n";
 $usermenu .= "<li><a href=\"https://sites.google.com/site/onlinezonejournals\" target=\"_blank\">Software Updates</a></li>\n";
 $usermenu .= "</ul>";
}

# Main Footer
$blogfooter .= "<em>$lang[50] $blogtitle $lang[51] $auth<br />\n";
$blogfooter .= "$blog<br />\n";
$blogfooter .= "$lang[52] $total $lang[53].<br /><br /></em>\n";
# THOU SHALT NOT REMOVE THE POWERED BY LINE... PRETTY PLEASE? ^_^
$blogfooter .= "$lang[54] <a href=\"https://sites.google.com/site/onlinezonejournals\" target=\"_blank\">OZJournals</a><br />\n";

?>