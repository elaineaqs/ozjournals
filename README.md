# README
OZJournals v3.2

### Contents 
* [Prerequisites](#prerequisites) 
* [Installation](#installation) 
* [Troubleshooting](#troubleshooting) 
* [Contributions](#contributions) 
* [About the Author](#about_the_author) 
* [Credits](#credits) 

### Prerequisites

<a name="prerequisites"></a> 
* PHP version 4.1 or higher. 
* PHP GD Library. 
* Ability to change file and directory permissions (CHMOD). 

### Installation
<a name="installation"></a>
1. Decompress the OZJournals ZIP file.

2. Set the following file permissions (for Linux servers):

	* CHMOD the following folders/ directories to 0777: 
		* archives 
		* comments 
		* lang 
		* pages 
		* posts 
		* themes 

	* CHMOD the following files to 0666: 
		* ads.php 
		* categories.php 
		* config.php 
		* counter.php 
		* usersdb.php 

3. Open your web browser and browse to the directory where the OZJournals files are located.

4. You may now start using your blog.
Default Admin username is "me" (without the double quotations) and the default Admin password is also "me" (without the double quotations.) 

### Important Notes
<a name="importantnotes"></a> 

* Click Blog Settings to modify the default configuration.

* **Make sure to change the Blog URL in Blog Settings to the appropriate URL for your website to avoid problems with the comment system.** 

* Only the 'mailto' system has been configured to send mails. If your server does not have 'mailto' functionality, e-mail messages or alerts will not be sent to the e-mail address in your Blog Settings. 

### Troubleshooting
<a name="troubleshooting"></a> 
If there is a problem with file permissions, I suggest you set writable permissions to the main directory (chmod 777). Problems with permissions for blogs running on Linux servers may occur since I used the Windows OS while running and testing the scripts. With Windows operating systems, there's no option for file permissions in web servers. For other bugs, errors, questions and comments, please email them to me. 

If files are not created and/or file permissions are not set, try to manually do the following: 

1. Create the following folders/ directories: 
	* archives 
	* comments 
	* pages 
	* posts

2. CHMOD the following folders/ directories to 0777: 
	* archives 
	* comments 
	* lang 
	* pages 
	* posts 
	* themes

3. Create the following files: 
	* ads.php 
	* categories.php 
	* counter.php 
	* usersdb.php

4. CHMOD the following files to 0666: 
	* ads.php 
	* categories.php 
	* config.php 
	* counter.php 
	* usersdb.php 

### Contributions
<a name="contributions"></a>
Contributions in any form are much appreciated. To contribute, contact me through email: [elaineaqs@gmail.com](mailto:elaineaqs@gmail.com).

Examples of contributions might be script plug-ins, bug reports, documentations, themes, language additions, etc. 

### About the Author
<a name="about_the_author"></a>
Please visit [OZJournals](https://sites.google.com/site/onlinezonejournals) for more information about the author. 

### Credits
<a name="credits"></a>
Thank you to the people who have downloaded OZJournals v3.2! Thank you also to those who have downloaded the previous versions. I know you're just too nice to tell me how it sucks sometimes. Or maybe most of the time. Hee hee. ^\_^ 

But for your non-violent reactions or perhaps for some answers to questions about the script, try to visit the main website for OZJournals at [sites.google.com/site/onlinezonejournals](https://sites.google.com/site/onlinezonejournals).


&copy; Copyright 2006-2011 OZJournals version 3.2

