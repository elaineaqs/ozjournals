<?php
header("Content-type: image/png");
session_start();
$im = @imagecreate(80, 20)
    or die("Cannot Initialize new GD image stream");
$background_color = imagecolorallocate($im, 255, 255, 255);
$text_color = imagecolorallocate($im, 233, 14, 91);
imagestring($im, 5, 16, 2,  $_SESSION['verify'], $text_color);
imagepng($im);
imagedestroy($im);
?> 