<?php

/********************************************************************************************************

   OZJournals Version 3.2 released by Online Zone <https://sites.google.com/site/onlinezonejournals>
   Copyright (C) 2006-2011 Elaine Aquino <elaineaqs@gmail.com>

   This program is free software; you can redistribute it and/or modify it 
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or (at your option) 
   any later version.

********************************************************************************************************/

# For the session
ob_start(); 

# For function file inclusion
include "functions.php"; 

# For language file inclusion
$langfile = file_get_contents("lang/index.php");
$eachlang = explode("\t", $langfile);
$oklang = $eachlang[1];
$uselang = file_get_contents("lang/$oklang.php");
$lang = explode("\n", $uselang);

# Clean the GET variablezzz
$clean['show'] = string_filter_nospace($_GET['show']);
$clean['id'] = int_filter($_GET['id']);
$clean['p'] = string_filter_nospace($_GET['p']);
$clean['f'] = int_filter($_GET['f']);
$clean['pn'] = int_filter($_GET['pn']);

# Printer-friendly view 
if($clean['show'] == "printpreview") {
 if(isset($clean['id'])) {
  $id = super_filter($clean['id']);
  if(file_exists("$datadirectory/$id.php")) {
   $pfile = file_get_contents("$datadirectory/$id.php");
   $what = explode("\t", $pfile);
   die("<html><head><title>$what[1]</title></head><body onload=\"window.print()\"><h1>$what[1]</h1><h5>Posted by $what[4] [ $what[5] ]<br />Date: ".date($timeformat, $what[2])."<br />Category: $what[6]</h5><p>$what[3]</p><h5>Source URL: <script>document.writeln(self.location)</script></h5></body></html>");
  }
 }
}

# RSS view
if($clean['show'] == "rss") {
 die(generaterss());
}

# Theme Picker
$themefile = file_get_contents("themes/index.php");
$eachtheme = explode("\t", $themefile);
$oktheme = $eachtheme[1];

include "themes/$oktheme/advanced.php";
$header = implode(" ", file("themes/$oktheme/header.php"));
$header = str_replace("{blogtitle}", $blogtitle, $header);
$header = str_replace("{subtitle}", subtitle($clean["show"], $clean["p"], $clean["f"]), $header);
$header = str_replace("{metakeywords}", $metakeywords, $header);
$header = str_replace("{metadescription}", $metadescription, $header);
$header = str_replace("{author}", $auth, $header);
$header = str_replace("{header}", $blogheader, $header);
$header = str_replace("{mainlinks}", $mainlinks, $header);
$header = str_replace("{add-ons}", $addons, $header);
$header = str_replace("{search}", $search, $header);
$header = str_replace("{categories}", $categories, $header);
$header = str_replace("{archives}", $archives, $header);
$header = str_replace("{miscellaneous}", $miscellaneous, $header);
$header = str_replace("{adminmenu}", "", $header);
$header = str_replace("{usermenu}", "", $header);
$header = str_replace("{footer}", $blogfooter, $header);
echo $header;

# Signing out Initial
if($clean['show'] == "signout") {
 session_start();
 session_destroy();
 header("Location: index.php?show=signout2");
}

# Signing out Part 2
if($clean['show'] == "signout2") {
 adminpanel("$lang[1]<br /><br />");
}

# Sign-in
elseif($clean['show'] == "signin") {
 echo "<h3>$lang[2]</h3>\n";
 echo "<form name=\"e\" method=\"post\" action=\"index.php?show=signinok\">\n";
 echo "<table border=\"0\">\n";
 echo "<tr><td width=\"40%\">$lang[3]</td>";
 echo "<td width=\"60%\"><input type=\"radio\" name=\"mode\" value=\"admin\" checked=\"checked\" /> $lang[4] \n";
 echo "<input type=\"radio\" name=\"mode\" value=\"user\"> $lang[5] </td></tr>\n";
 echo "<tr><td width=\"40%\"> $lang[6] </td>";
 echo "<td width=\"60%\"><input type=\"text\" name=\"username\" /></td></tr>\n";
 echo "<tr><td width=\"40%\"> $lang[7] </td>";
 echo "<td width=\"60%\"><input type=\"password\" name=\"password\" /></td></tr>\n";
 echo "</table><br />\n";
 echo "<button type=\"submit\"> $lang[8] </button>&nbsp; \n";
 echo "<button type=\"reset\"> $lang[9] </button> \n";
 echo "</form><br /><br />\n";
}

# Blog
elseif($clean['show'] == "" OR $clean['show'] == "main" OR $clean['show'] == "index") {
 $fortitle=$tagline;
 $pn = $clean['pn'];
 if($total > 0) {
  if($total >= $displaynumber) {
   if($pn == "") {
    $pn = $total;
    echo display ($displaynumber, $datadirectory, $total, $pn); 
    echo "<p> &nbsp; </p>\n";
   }
   else { 
    $pn = $clean['pn'];
    echo display ($displaynumber, $datadirectory, $total, $pn);
    echo "<p> &nbsp; </p>\n";
   }
  }
  else {
   echo display ($displaynumber, $datadirectory, $total, $pn);
   echo "<p> &nbsp; </p>\n";
  }
 }
 else {
  blogviewer("$lang[10]<br />\n\n", $blogtitle);
 }
}

# Pages
elseif($clean['show'] == "pages") {
 $totalpages = countfiles("pages");
 $currentpage = super_filter($clean['f']);
 if(file_exists("pages/$currentpage.php")) {
  $cc = file_get_contents("pages/$currentpage.php");
  $eachcc = explode("\t", $cc);
  if($totalpages > 0) {
   echo "<h3>".$eachcc[1]."</h3>\n";
   echo "<h5>$lang[12] <a href=\"".$eachcc[5]."\">".$eachcc[4]."</a> $lang[11] ".date($timeformat, $eachcc[2])." </h5>\n";
   echo "<p> ".stripslashes($eachcc[3])." </p>\n\n";
  }
  else {
   blogviewer("$lang[13]<br />\n\n", $blogtitle);
  }
 }
 else {
  blogviewer("$lang[81]<br />\n\n", $lang[80]);
 }
}

# Complete archives
elseif($clean['show'] == "archives") {
 if($total > 0) {
  $theyear = int_filter($_GET['y']);
  $themonth = $_GET['m'];
  $thecategory = $_GET['c'];
  $p = $clean['p'];
  if($p == "main") {
   echo adisplayall ($datadirectory, $total);
  }
  else {
   if(isset($theyear) AND isset($themonth)) {
    echo adisplaymonthandyear ($datadirectory, $total, $themonth, $theyear);
   }
   elseif(isset($thecategory)) {
    echo adisplaycategory ($datadirectory, $total, $thecategory);
   }
   elseif(file_exists("$datadirectory/$p.php")) {
    for($e=1; $e <= $total; $e++) {
     if($p == $e) {
      echo displayeach($datadirectory, $p);
     }
    }
   }
   else {
    blogviewer("$lang[81]<br />\n\n", $lang[80]);
   }
  }
 }
 else {
  blogviewer("$lang[14]<br />\n\n", $blogtitle);
 }
}

# Comment poster
elseif($clean['show'] == "postcomments") {
 $date = date_and_time($timezone, "", "", "", "", "", "", "");
 $name = string_filter($_POST['vname']);
 $vemail = string_filter_email($_POST['vemail']);
 $location = string_filter_url($_POST['vlocation']);
 $comment = string_filter_comment($_POST['vcomment']);
 $verify = string_filter_nospace($_POST['verify']);
 $p = $clean['p'];
 // (1) Block if there's no name, email and comment
 if($name !== "" && $vemail !== "" && $comment !== "") {
  // (2) Block if it doesn't have a valid email addie
  if(ValidEmail($vemail)) {
   $filecont = file_get_contents("comments/$p.php");
   if($location == "") {
    $all = "<?php /*\t".str_replace("\r\n", "<br />", $comment)."\t".$name."\t".$date."\tnone\t".$vemail."\t*/ ?>\n";
   }
   else {
    if(substr($location, 0, 7)!=="http://") {
     $all = "<?php /*\t".str_replace("\r\n", "<br />", $comment)."\t".$name."\t".$date."\thttp://".$location."\t".$vemail."\t*/ ?>\n";
    }
    else {
     $all = "<?php /*\t".str_replace("\r\n", "<br />", $comment)."\t".$name."\t".$date."\t".$location."\t".$vemail."\t*/ ?>\n";
    }
   }
   // (3) Block if the referrer is different from blog address in settings
   if($_SERVER["HTTP_REFERER"]=="$blog/index.php?show=archives&p=$p") {
    // (4) Verify session first before posting the comment.
    if($verify!=="") {
     session_start();
	 if($verify==$_SESSION['verify']) {
	  // (5) Block comment if it contains bad words
	  if(!strpos($comment, $badwords)) {
	   // (6) Do not post comment if the exact same comment and name already exists.
	   if(!preg_match("/\\t$comment\\t$name/", file_get_contents("comments/$p.php"))) {
       $handle = fopen ("comments/$p.php", "a+");
       fwrite ($handle, $all);
	   fclose ($handle);
	   // Email after comment has been posted. Negative (-) if not emailed.
	   if($emailcomments!=="yes") {
	    blogviewer($name.", $lang[15]<br /><a href=\"index.php?show=archives&p=".$p."\">$lang[16]</a>. -<br />\n\n", $blogtitle);
	   }
	   else {
	    $comment = str_replace("<br />", "\r\n", $comment);
	    if(mail($vemail, $lang[84], "$lang[85] $user,\r\n\r\n$name ($vemail) $lang[86] ($blogtitle). $lang[87]:\r\n\r\n------------\r\n\r\n$comment\r\n\r\n------------\r\n\r\n$lang[88]:\r\n$blog/admin.php?show=editdeletecomments\r\n\r\n$lang[89]\r\n\r\n$lang[90]", "From: $blogtitle <$email>")) {
	     blogviewer($name.", $lang[15]<br /><a href=\"index.php?show=archives&p=".$p."\">$lang[16]</a>. +<br />\n\n", $blogtitle);
	    }
	    else {
	     blogviewer($name.", $lang[15]<br /><a href=\"index.php?show=archives&p=".$p."\">$lang[16]</a>. -<br />\n\n", $blogtitle);
	    }
	   }
	   }
	   else {
	    blogviewer("$name, $lang[92]<br /><a href=\"javascript: history.go(-1);\">$lang[21]</a><br />\n", $blogtitle);
	   }
	  }
	  else {
	   blogviewer("$name, $lang[93]<br /><a href=\"javascript: history.go(-1);\">$lang[21]</a><br />\n", $blogtitle);
	  }
	 }
	 else {
	  blogviewer("$lang[17]<br /><a href=\"javascript: history.go(-1);\">$lang[21]</a><br />\n", $blogtitle);
	  $session_degenerate_id;
	 }
    }
    else {
     blogviewer("$lang[18]<br /><a href=\"javascript: history.go(-1);\">$lang[21]</a><br />\n", $blogtitle);
	 $session_degenerate_id;
    }
   }
   else {
    blogviewer("$lang[19]<br /><a href=\"javascript: history.go(-1);\">$lang[21]</a><br />\n", $blogtitle);
	$session_degenerate_id;
   }
  }	
  else {
   blogviewer("$lang[20]<br /><br /><a href=\"javascript: history.go(-1);\">$lang[21]</a><br />\n", "$blogtitle");
   $session_degenerate_id;
  }
 }
 else {
  blogviewer("$lang[22] <b>$lang[23]</b>, <b>$lang[24]</b>, $lang[25] <b>$lang[26]</b>.<br /> $lang[27]<br /><br /><a href=\"javascript: history.go(-1);\">$lang[21]</a>", "$blogtitle");
 }
}

# Signed in
elseif($clean['show'] == "signinok") {
 $mode = string_filter_nospace($_POST['mode']);
 if($_POST['username'] !== '' AND $_POST['password'] !== '') {
  # Administrator Mode
  if($mode == "admin") {
   $newsessionuser = md5($_POST['username'].mktime());
   $newsessionpass = md5($_POST['password'].mktime());
   if($newsessionuser == md5($user.mktime()) AND $newsessionpass == md5($pass.mktime())) {
    session_destroy();
    session_start();
    $_SESSION['user'] = $user;
    $_SESSION['mode']  = "admin";
    adminpanel("$lang[28], <b>$auth</b>! <br /><br /> $lang[29]<br />$lang[30] <a href=\"admin.php\">$lang[31]</a>.<br />\n\n");
	session_write_close();
   }
   else {
    adminpanel ("$lang[32]<br />$lang[33] <a href=\"index.php?show=signin\">$lang[34]</a>.<br />\n\n");
	$session_degenerate_id;
   }
  }
  # User Mode
  elseif($mode == "user") {
   $usersdb = file_get_contents("usersdb.php");
   $ida = explode($_POST['username']."\t", $usersdb);
   $idb = str_replace("<?php /*\t", "", $ida[0]);
   $idc = $idb - 1;
   $passa = explode("\n", $usersdb);
   $passb = explode("\t", $passa[$idc]);
   $passc = $passb[4];
   if(strpos($usersdb, $_POST['username']) !== FALSE AND $passc == md5($_POST['password'])) {
    session_destroy();
    session_start();
    $_SESSION['user'] = $_POST['username'];
    $_SESSION['mode']  = "user";
    blogviewer("$lang[28], <b>".$_SESSION['user']."</b>! <br /><br /> $lang[29]<br /> $lang[30] <a href=\"admin.php\">$lang[35]</a>.<br />\n\n", "User's Panel");
	session_write_close();
   }
   else {
    blogviewer("$lang[32]<br />$lang[33] <a href=\"index.php?show=signin\">$lang[34]</a>.<br />\n\n", "User's Panel");
	$session_degenerate_id;
   }
  }
  # Anything else is invalid
  else {
   blogviewer("$lang[83] <br />\n\n", $lang[82]);
  }
 }
 else {
  blogviewer("$lang[36] <br />$lang[33] <a href=\"index.php?show=signin\">$lang[34]</a>.<br />\n\n", $lang[82]);
 }
}

# Load 404 Page
else {
 blogviewer("$lang[81]<br />\n\n", $lang[80]);
}

# Simple statistics
if(file_exists("counter.php")) {
  $counterfile = file_get_contents("counter.php");
  $counterdata = explode("|", $counterfile);
  $countermagic = count($counterdata);
  $h = fopen("counter.php", "a+");
  fwrite($h, $_SERVER['REMOTE_ADDR']."|");
}

# Footer
$footer = implode(" ", file("themes/$oktheme/footer.php"));
$footer = str_replace("{blogtitle}", $blogtitle, $footer);
$header = str_replace("{subtitle}", subtitle($clean["show"], $clean["p"], $clean["f"]), $header);
$footer = str_replace("{metakeywords}", $metakeywords, $footer);
$footer = str_replace("{metadescription}", $metadescription, $footer);
$header = str_replace("{author}", $auth, $header);
$footer = str_replace("{header}", $blogheader, $footer);
$footer = str_replace("{mainlinks}", $mainlinks, $footer);
$footer = str_replace("{add-ons}", $addons, $footer);
$footer = str_replace("{search}", $search, $footer);
$footer = str_replace("{categories}", $categories, $footer);
$footer = str_replace("{archives}", $archives, $footer);
$footer = str_replace("{miscellaneous}", $miscellaneous, $footer);
$footer = str_replace("{adminmenu}", "", $footer);
$footer = str_replace("{usermenu}", "", $footer);
$footer = str_replace("{footer}", $blogfooter, $footer);
echo $footer;

# End flush for the session
ob_end_flush(); 

?>