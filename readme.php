<?php

/********************************************************************************************************

   OZJournals Version 3.2 released by Online Zone <https://sites.google.com/site/onlinezonejournals>
   Copyright (C) 2006-2011 Elaine Aquino <elaineaqs@gmail.com>

   This program is free software; you can redistribute it and/or modify it 
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or (at your option) 
   any later version.

********************************************************************************************************/

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="description" content="Test" />
 <meta name="keywords" content="a journal by Tester" />
 <meta name="author" content="Tester" />
 <title> OZJournals ReadMe File </title>
 <style type="text/css">
 <!--
 @import url("themes/default/zitemplate00.css");
 -->
 </style>
 </head>
 <body>
 <div id="container">
 <!-- header -->
 <div id="header">
 <a href="readme.php"><h1> ReadMe File </h1></a><p class="slogan"> OZJournals version 3.2 </p>
 </div>
 <!-- main menu -->
 <div id="mainmenu">
 <ul>
 <li>&nbsp;</li> 
 </ul>
 </div>
 <!-- content area -->
 <div id="content">
 <table>
 <tr>
 <td width="200" valign="top">
 <h4> Menu </h4>
 <ul>
 <li><a href="#prerequisites">Prerequisites</a></li>
 <li><a href="#installation">Installation</a></li>
 <li><a href="#troubleshooting">Troubleshooting</a></li>
 <li><a href="#contributions">Contributions</a></li>
 <li><a href="#about_the_author">About the Author</a></li>
 <li><a href="#credits">Credits</a></li>
 <br /><br />
 <li><a href="index.php">Home</a></li>
 <li><a href="admin.php">Admin</a></li>
 </ul>
 </td>
 <td width="560" valign="top">
 <h2>OZJournals 3.2 ReadMe File</h2>
 
 <h3>Prerequisites<a name="prerequisites"></a></h3>
 <ul type="disc">
 <li> PHP version 4.1 or higher. </li>
 <li> PHP GD Library. </li>
 <li> Ability to change file and directory permissions (CHMOD). </li>
 </ul>

 <br /><h3>Installation<a name="installation"></a></h3>
 <ol type="1">
 <li> Decompress the OZJournals ZIP file. <br /><br /></li>
 <li> Set the following file permissions (for Linux servers):
 <p>CHMOD the following folders/ directories to 0777:</p>
 <ul>
 <li>archives</li>
 <li>comments</li>
 <li>lang</li>
 <li>pages</li>
 <li>posts</li>
 <li>themes</li>
 </ul>
 <p>CHMOD the following files to 0666:</p>
 <ul>
 <li>ads.php</li>
 <li>categories.php</li>
 <li>config.php</li>
 <li>counter.php</li>
 <li>usersdb.php</li>
 </ul>
 </li>
 <li> Open your web browser and browse to the directory where the OZJournals files are located. <br /><br /></li>
 <li> You may now start using your blog.<br />
 Default Admin username is "me" (without the double quotations)
 and the default Admin password is also "me" (without the double quotations.)<br /><br /></li>
 </ol>
 
 <br /><h3>Important Notes<a name="importantnotes"></a></h3>
 <ul>
 <li>Click Blog Settings to modify the default configuration.<br /><br /></li>
 <li><b>Make sure to change the Blog URL in Blog Settings to the appropriate URL for your website to avoid problems with the comment system.</b><br /><br /></li>
 <li>Only the 'mailto' system has been configured to send mails. If your server does not have 'mailto' functionality, e-mail messages or alerts will not be sent to the e-mail address in your Blog Settings.</li>
 </ul>
 
 <br /><h3>Troubleshooting<a name="troubleshooting"></a></h3>
 <p>If there is a problem with file permissions, I suggest you
 set writable permissions to the main directory (chmod 777).
 Problems with permissions for blogs running on Linux servers may occur since I used the Windows OS while
 running and testing the scripts. With Windows operating systems,
 there's no option for file permissions in web servers. For other bugs, errors,
 questions and comments, please email them to me. </p>
 
 <p>If files are not created and/or file permissions are not set, try to manually do the following:<br /><br />
 Create the following folders/ directories:
 <ul>
 <li>archives</li>
 <li>comments</li>
 <li>pages</li>
 <li>posts</li>
 </ul>
 CHMOD the following folders/ directories to 0777:
 <ul>
 <li>archives</li>
 <li>comments</li>
 <li>lang</li>
 <li>pages</li>
 <li>posts</li>
 <li>themes</li>
 </ul>
 Create the following files:
 <ul>
 <li>ads.php</li>
 <li>categories.php</li>
 <li>counter.php</li>
 <li>usersdb.php</li>
 </ul>
 CHMOD the following files to 0666:
 <ul>
 <li>ads.php</li>
 <li>categories.php</li>
 <li>config.php</li>
 <li>counter.php</li>
 <li>usersdb.php</li>
 </ul>
 </p>

 <br /><h3>Contributions<a name="contributions"></a></h3>
 <p>Contributions in any form are much appreciated.
 To contribute, contact me through email: 
 <a href="mailto:elaineaqs@gmail.com">elaineaqs@gmail.com</a>.</p>
 <p>Examples of contributions might be script plug-ins, bug reports,
 documentations, themes, language additions, etc. </p>

 <br /><h3>About the Author<a name="about_the_author"></a></h3>
 <p>Please visit <a href="https://sites.google.com/site/onlinezonejournals" target="_blank">OZJournals</a>
 for more information about the author.</p>

 <br /><h3>Credits<a name="credits"></a></h3>
 <p>Thank you to the people who have downloaded OZJournals v3.2! Thank you also to 
 those who have downloaded the previous versions. I know you're just too nice to tell me how it sucks sometimes.
 Or maybe most of the time. Hee hee. ^_^</p>
 <p>But for your non-violent reactions or perhaps for some answers to questions 
 about the script, try to visit the main website for OZJournals at
 <a href="https://sites.google.com/site/onlinezonejournals" target="_blank">sites.google.com/site/onlinezonejournals</a>.</p>
 <br /><br /><br /><br />
 </td>
 </tr>
 </table>
 </div>
 <!-- content area -->
 <div id="footer" align="center">
 &copy; Copyright 2006-2011 OZJournals version 3.2<br /><br />
 Powered by <a href="https://sites.google.com/site/onlinezonejournals" target="_blank">OZjournals</a><br />
 </div>
 </div>
 </body>
 </html>