<?php

/********************************************************************************************************

   OZJournals Version 3.2 released by Online Zone <https://sites.google.com/site/onlinezonejournals>
   Copyright (C) 2006-2011 Elaine Aquino <elaineaqs@gmail.com>

   This program is free software; you can redistribute it and/or modify it 
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or (at your option) 
   any later version.

********************************************************************************************************/

# For function file inclusion
include "functions.php"; 

# Check the session
check_login(); 

# For language file inclusion
$langfile = file_get_contents("lang/index.php");
$eachlang = explode("\t", $langfile);
$oklang = $eachlang[1];
$uselang = file_get_contents("lang/$oklang.php");
$lang = explode("\n", $uselang);

# Clean the GET variablezzz
$clean['show'] = string_filter_nospace($_GET['show']);
$clean['id'] = string_filter_nospace($_GET['id']);
$clean['p'] = string_filter_nospace($_GET['p']);
$clean['f'] = int_filter($_GET['f']);
$clean['pn'] = int_filter($_GET['pn']);
$clean['n'] = int_filter($_GET['n']);

# Theme Picker
$themefile = file_get_contents("themes/index.php");
$eachtheme = explode("\t", $themefile);
$oktheme = $eachtheme[1];

include "themes/$oktheme/advanced.php";
$header = implode(" ", file("themes/$oktheme/header.php"));
$header = str_replace("{blogtitle}", $blogtitle, $header);
$header = str_replace("{subtitle}", subtitle($clean["show"], $clean["p"], $clean["f"]), $header);
$header = str_replace("{metakeywords}", $metakeywords, $header);
$header = str_replace("{metadescription}", $metadescription, $header);
$header = str_replace("{author}", $auth, $header);
$header = str_replace("{header}", $blogheader, $header);
$header = str_replace("{mainlinks}", $mainlinks, $header);
$header = str_replace("{add-ons}", "", $header);
$header = str_replace("{search}", "", $header);
$header = str_replace("{categories}", "", $header);
$header = str_replace("{archives}", "", $header);
$header = str_replace("{miscellaneous}", "", $header);
$header = str_replace("{adminmenu}", $adminmenu, $header);
$header = str_replace("{usermenu}", $usermenu, $header);
$header = str_replace("{footer}", $blogfooter, $header);
echo $header;

# Insert the JavaScript thing ?>
<script language="javascript" type="text/javascript" src="tinymce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_deselector : "mceNoEditor",
	theme_advanced_buttons1 : "bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink,separator,image,cleanup,help,code",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_path_location : "bottom",
	extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
});
</script>

<?php
session_start();

if($_SESSION['mode'] == "admin") {

 # Control panel page
 if($clean['show'] == "" OR $clean['show'] == "main") {
  echo "<h3>Welcome to the admin's panel! &Uuml;</h3>\n";
  echo "<p><b>Greetings, <em>".$auth."</em>!</b><br /> \n";
  echo "Please select a link below so you can start managing your blog.</p>\n\n";
  echo "<table cellspacing=8 cellpadding=8 border=0 width=\"100%\">\n";
  echo "<tr><td width=\"30%\"><b>Write</b>: </td>\n";
  echo "<td width=\"70%\"><a href=\"admin.php?show=add\">Journals</a> \n";
  echo " - <a href=\"admin.php?show=addpages\">Pages</a> \n";
  echo " - <a href=\"admin.php?show=addcategories\">Categories</a></td></tr>\n";
  echo "<tr><td width=\"10%\"><b>Change</b>: </td>\n";
  echo "<td width=\"90%\"><a href=\"admin.php?show=editconfig\">Blog Settings</a> \n";
  echo " - <a href=\"admin.php?show=themes\">Themes</a> \n";
  echo " - <a href=\"admin.php?show=userconfig\">User Accounts</a></td></tr>\n";
  echo "<tr><td width=\"10%\"><b>Edit/Delete</b>: </td>\n";
  echo "<td width=\"90%\"><a href=\"admin.php?show=editjournals\">Journals</a> \n";
  echo " - <a href=\"admin.php?show=editpages\">Pages</a> \n";
  echo " - <a href=\"admin.php?show=editcategories\">Categories</a> <br /> \n";
  echo " <a href=\"admin.php?show=editdeletecomments\">Comments</a> \n";
  echo " - <a href=\"admin.php?show=editarchives\">Archives</a> \n";
  echo " - <a href=\"admin.php?show=editads\">Add-ons</a></td></tr>\n";
  echo "<tr><td width=\"10%\"><b>View</b>: </td>\n";
  echo "<td width=\"90%\"><a href=\"admin.php?show=statistics\">Statistics</a></td></tr>\n";
  echo "<tr><td width=\"10%\"><b>OZJournals</b>: </td>\n";
  echo "<td width=\"90%\"><a href=\"readme.php\">Readme</a> \n";
  echo " - <a href=\"license.txt\">License</a> \n";
  echo " - <a href=\"https://sites.google.com/site/onlinezonejournals\" target=\"_blank\">Software Updates</a></td></tr>\n";
  echo "</table><br /><br />\n\n";
  echo "<br /><p>Have fun using OZJournals! <br /> For more information and downloads, <br /> please visit our site at \n";
  echo "<a href=\"https://sites.google.com/site/onlinezonejournals\" target=\"_blank\">https://sites.google.com/site/onlinezonejournals</a>.\n";
  echo "</p><br />\n\n";
 }
 
 # Page for adding posts
 if($clean['show'] == "add") {
  echo "<h3>Add a Journal Entry</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=post\" name=\"e\">\n";
  echo "<b>Status:</b> <input type=\"radio\" name=\"saveas\" value=\"draft\"> Draft | \n";
  echo "<input type=\"radio\" name=\"saveas\" value=\"published\" checked> Published &nbsp;&nbsp;&nbsp;\n\n";
  echo "<b>Category:</b> <select name=\"category\"><option value=\"No Category\" selected>No Category</option>\n";
  if(file_exists("categories.php")) {
   $categoryfile = file_get_contents("categories.php");
   $eachcategory = explode("\n", $categoryfile);
   $allcategories = count($eachcategory) - 1;
   for($c = 0; $c < $allcategories; $c++) {
    $realc=explode("\t", $eachcategory[$c]);
    echo "<option value=\"".tospace($realc[1])."\">".tospace($realc[1])."</option>\n";
   }
  }
  else {
   echo "<li>The <code>categories</code> file does not exist.";
  }
  echo "</select><br /><br />\n\n";
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\" checked> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\"> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" /> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
  echo "<b>TITLE</b>: <input type=\"text\" name=\"thetitle\" size=\"40\" maxlength=\"50\" /><br /><br /> \n\n";
  echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\"></textarea>\n\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "<input type=\"hidden\" name=\"hidden\" value=\"".countfiles('posts')."\">\n";
  echo "</div></form><br />\n";
 }

 # Now add the post to database
 if($clean['show'] == "post") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
  $t = $_POST["thetitle"];
  $m = $_POST["themessage"];
  $c = $_POST["category"];
  $s = $_POST["saveas"];
  $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$auth."\t".$email."\t".$c."\t".$s."\t*/ ?>";
  $hidden=$_POST['hidden'];
  $e=$hidden +1;
  $month = date_and_time($timezone, "F", "", "", "", "", "", "");
  $year = date_and_time($timezone, "Y", "", "", "", "", "", "");
  if($t !== "" && $m !== "") {
   if(!file_exists("archives/$year-$month.php")) {
    $yh = fopen("archives/$year-$month.php", "x");
   }
   $handle=fopen("$datadirectory/$e.php" , "w+");
   fwrite($handle, newlines($message));
   $handler=fopen("comments/$e.php" , "w+");
   fwrite($handler, "");
   adminpanel ("You have successfully added a journal entry!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=add\">Add another entry</a>");
  }
  else {
   adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this post.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Page for adding new front-end pages
 if($clean['show'] == "addpages") {
  echo "<h3>Add a Page</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=postpages\">\n";
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\" checked> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\"> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" /> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
  echo "<b>TITLE</b>: <input type=\"text\" name=\"thetitle\" size=40 maxlength=50> <br /><br />\n\n";        
  echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\"></textarea>\n\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "<input type=\"hidden\" name=\"hidden\" value=\"".countfiles('pages')."\">\n";
  echo "</div></form><br />\n";
 }

 # Now add those new pages to the database
 if($clean['show'] == "postpages") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
  $t=$_POST["thetitle"];
  $m=$_POST["themessage"];
  $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$auth."\t".$email."\t*/ ?>";
  $hidden=$_POST['hidden'];
  $e=$hidden +1;
  if($t !== "" && $m !== "") {
   $handle=fopen("pages/$e.php" , "w+");
   fwrite($handle, newlines($message));
   adminpanel ("You have successfully added a page!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=addpages\">Add another page</a>");
  }
  else {
   adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this page.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Page for adding categories 
 if($clean['show'] == "addcategories") {
  echo "<h3>Add a Category</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=postcategories\">\n";
  echo "Category Name <input type=\"text\" name=\"themessage\" size=30 maxlength=50>\n\n";        
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "</div>\n";
  echo "</form><br />\n";
 }

 # Now add those categories to the database
 if($clean['show'] == "postcategories") {
  $m=$_POST["themessage"];
  $message="<?php /*\t".todash($m)."\t*/ ?>\n";
  if($m !== "") {
   $handle=fopen("categories.php" , "a+");
   fwrite($handle, stripslashes($message));
   adminpanel ("You have successfully added a category!<br /><br /><a href=\"index.php\">View Blog</a><br />
        <a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=addcategories\">Add another category</a>");
  }
  else {
   adminpanel ("Please include a <b>NAME</b> for the new category.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Page for listing the posts to be edited or deleted
 if($clean['show'] == "editjournals") {
  if($total >0) {
   echo "<h3>Edit Journals</h3>\n";
   echo "<p>Please select a journal to edit or delete.<p>\n\n";
   $total = countfiles($datadirectory);
   $p=$clean['p'];
   if($p == "") {
    echo editdisplayalladmin ($datadirectory, $total);
   }
  }
  else {
   echo adminpanel ("No journals yet!<br /><br />");
  }
 }

 # Page for editing the post
 if($clean['show'] == "displayedit") {
  $jnumber = $clean['n'];
  $file = file_get_contents("$datadirectory/$jnumber.php");
  $each = explode("\t",$file);
  echo "<h3>Editor for ".$each[1]." content</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=goedit&n=".$jnumber."\">\n";
  echo "<b>Status:</b> <input type=\"radio\" name=\"saveas\" value=\"draft\" ";
  if($each[7] == "draft") {
   echo " checked";
  }
  echo "> Draft | \n";
  echo "<input type=\"radio\" name=\"saveas\" value=\"published\" ";
  if($each[7] == "published") {
   echo " checked";
  }
  echo "> Published &nbsp;&nbsp;&nbsp;\n\n";
  echo "<b>Category:</b> <select name=\"category\"><option value=\"No Category\" ";
  if($each[6] == "No Category") {
   echo " selected";
  }
  echo ">No Category</option>\n";
  if(file_exists("categories.php")) {
   $categoryfile = file_get_contents("categories.php");
   $eachcategory = explode("\n", $categoryfile);
   $allcategories = count($eachcategory) - 1;
   for($c = 0; $c < $allcategories; $c++) {
    $eachc = explode("\n", $categoryfile);
    $eachrealc = explode("\t", $eachc[$c]);	   
    echo "<option value=\"".tospace($eachrealc[1])."\" ";
    if($each[6] == tospace($eachrealc[1])) {
	 echo " selected";
	}
    echo ">".tospace($eachrealc[1])."</option>\n";
   }
  }
  else {
   echo "<li>The <code>categories</code> file does not exist.";
  }
  echo "</select><br /><br />\n\n";        
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\"> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\" checked> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" value=\"";
  echo date("m-d-Y-H-i", $each[2]);
  echo "\"/> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
  echo "<b>TITLE</b>: <input type=\"text\" name=\"thetitle\" value=\"$each[1]\" size=40 maxlength=50> <br /><br />\n";        
  echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\">".editor($each[3])."</textarea>\n\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "</div></form><br />\n";
 }

 # Now edit the post and save to database
 if($clean['show'] == "goedit") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
  $t = $_POST['thetitle'];
  $m = $_POST['themessage']; 
  $w = $clean['n'];
  $c = $_POST['category'];
  $s = $_POST['saveas'];
  $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$auth."\t".$email."\t".$c."\t".$s."\t*/ ?>";
  $month = date_and_time($timezone, "F", "", "", "", "", "", "");
  $year =  date_and_time($timezone, "Y", "", "", "", "", "", "");
  if($t !== "" && $m !== "") {
   if(!file_exists("archives/$year-$month.php")) {
    $yh = fopen("archives/$year-$month.php", "x");
   }
   $fhandle = fopen("$datadirectory/$w.php", "w+");
   fwrite($fhandle,newlines($message));
   adminpanel ("The journal was modified successfully!<br /><br /><a href=\"index.php?show=archives&p=".$w."\"> View my Journal</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editjournals\"> Edit more Journals</a>.");
  }
  else {
   adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this post.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Displays a list of the pages to be edited or deleted
 if($clean['show'] == "editpages") {
  $totalpages = countfiles("pages");
  if($totalpages >0) {
   echo "<h3>Edit Pages</h3>\n";
   echo "<p>Please select a static page to edit or delete.<p>\n\n";
   echo "<ol type=\"1\">\n";
   $dir = "pages/";
   if(is_dir($dir)) {
    if($dh = opendir($dir)) {
     while(($file = readdir($dh)) !== false) {
      if($file !== "." AND $file !== "..") {
       if(filetype($dir.$file) !== "dir") {
        $pc = file_get_contents($dir.$file);
        $eachpc = explode("\t", $pc);
        echo "<li> <a href=\"admin.php?show=displayeditpages&f=".substr($file, 0, 1)."\">Edit</a> | \n";
        echo "<a href=\"admin.php?show=displaydeletepages&f=".substr($file, 0, 1)."\">Delete</a> \n";
        echo "<b>".$eachpc[1]."</b>\n\n";
       }
      }
     }
     closedir($dh);
     echo "</ol><br />\n\n";
    }
   }
  }
  else {
   echo adminpanel ("No pages yet! <br /><br />");
  }
 }

 # Displays a page for editing the selected pages to be edited... o_0 The redundancy of it all...
 if($clean['show'] == "displayeditpages") {
  $pn = $clean['f'];
  $filepc = file_get_contents("pages/$pn.php");
  $datapc = explode("\t",$filepc);
  echo "<h3>Editor for ".$datapc[1]." content</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=goeditpages&n=".$pn."\">\n";
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\"> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\" checked> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" value=\"";
  echo date("m-d-Y-H-i", $datapc[2]);
  echo "\"/> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
  echo "<b>TITLE</b>: <input type=\"text\" name=\"thetitle\" value=\"$datapc[1]\" size=40 maxlength=50> <br /><br />\n\n";        
  echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\">".editor($datapc[3])."</textarea>\n\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "</div>\n";
  echo "</form><br />\n";
 }

 # Now edit the page and save to database
 if($clean['show'] == "goeditpages") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
  $t = $_POST['thetitle'];
  $m = $_POST['themessage']; 
  $w = $clean['n'];
  $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$auth."\t".$email."\t*/ ?>";
  if($t !== "" && $m !== "") {
   $fhandle = fopen("pages/$w.php", "w+");        
   fwrite($fhandle,newlines($message));
   adminpanel ("The page was modified successfully!<br /><br /><a href=\"index.php?show=pages&f=".$w."\"> View Page</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editpages\"> Edit more Pages</a>.");
  }
  else {
   adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this page.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Lists the categories to be edited or deleted
 if($clean['show'] == "editcategories") {
  echo "<h3>Edit Categories</h3>\n";
  echo "<p>Please select a category to edit or delete.<p>\n\n";
  echo "<ol type=\"1\">\n";
  if(file_exists("categories.php")) {
   $cfile = file_get_contents("categories.php");
   $cdata = explode("\n", $cfile);
   $ccount = count($cdata) - 1;
   for($c = 0; $c < $ccount; $c++) {
    $eachc = explode("\n", $cfile);
    $eachrealc = explode("\t", $eachc[$c]);   
    echo "<li> <a href=\"admin.php?show=displayeditcategories&c=".$eachrealc[1]."\">Edit</a> | \n";
    echo "<a href=\"admin.php?show=displaydeletecategories&c=".$eachrealc[1]."\">Delete</a> \n";
    echo "<b>".tospace($eachrealc[1])."</b>\n\n";
   }
  }
  else {
   echo adminpanel ("There are no categories yet.<br /><br />");
  }
  echo "</ol><br />\n\n";
 }

 # Page for editing a category
 if($clean['show'] == "displayeditcategories") {
  $cate = $_GET['c'];
  echo "<h3>Editor for ".tospace($cate)." </h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=goeditcategories&c=".$cate."\">\n";
  echo "New Category Name <input type=\"text\" name=\"themessage\" value=\"".$cat."\" size=30 maxlength=50><br /><br />\n\n";        
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "</div>\n";
  echo "</form><br />\n";
 }

 # Now edit the category and save to database
 if($clean['show'] == "goeditcategories") {
  $c = $_GET['c'];
  $cname = $_POST['themessage'];
  $cfile = file_get_contents("categories.php");
  $forc = str_replace($c, todash($cname), $cfile);
  if($cname !== "") {
   $fhandle = fopen("categories.php", "w+");        
   fwrite($fhandle,stripslashes($forc));
   adminpanel ("The category was modified successfully!<br /><br /><a href=\"index.php\"> View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editcategories\"> Edit more Categories</a>.");
  }
  else {
   adminpanel ("Please include a <b>NAME</b> for the category.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Lists the archives to be edited or deleted
 if($clean['show'] == "editarchives") {
  echo "<h3>Edit Archives</h3>\n";
  echo "<p>Please select an archive to delete. <br />\n";
  echo "<b> This will delete all posts from the archive you select.</b></p>\n\n";
  echo "<ol type=\"1\">\n";
  $dir = "archives/";
  if(is_dir($dir)) {
   if($dh = opendir($dir)) {
    while(($file = readdir($dh)) !== false) {
     if(filetype($dir . $file) == "file") {
      if($file !== "." AND $file !== "..") {
       $dfile = explode("-", $file);
       $month = str_replace(".php", "", $dfile[1]);
       $year = $dfile[0];
       echo "<li><a href=\"admin.php?show=deletearchives&d=$year-$month\">";
       echo "Delete</a> $month $year \n";
      }
     }
    }
    closedir($dh);
    echo "</ol><br />\n\n";
   }
  }
  else {
   adminpanel ("There are no archives yet. <br />\n\n");
  }
 }

 # Page for editing the configuration settings
 if($clean['show'] == "editconfig") {
  echo "<h3>Configuration Settings</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=goconfig\">\n";
  echo "<table cellspacing=0 cellpadding=4 border=0 width=\"100%\">\n\n";
  echo "<tr><td width=\"40%\"> Blog Title </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"blogtitle\" value=\"".$blogtitle."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Tagline </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"tagline\" value=\"".$tagline."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Name of Journal Author </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"author\" value=\"".$auth."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Desired Username </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"username\" value=\"".$user."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Password </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"password\" value=\"".$pass."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Email Address </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"emailadd\" value=\"".str_replace("mailto:", "", $email)."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Blog URL </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"blogurl\" value=\"".$blog."\" size=30></td></tr>\n";
  echo "<tr><td width=\"100%\" colspan=2><br /><h3>Date and Time</h3></td></tr>\n";
  echo "<tr><td width=\"40%\"> Timezone </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"timezone\" value=\"".$timezone."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Time Format </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"timeformat\" value=\"".$timeformat."\" size=30></td></tr>\n";
  echo "<tr><td width=\"100%\" colspan=2><br /><h3>Meta Tags</h3></td></tr>\n";
  echo "<tr><td width=\"40%\"> Meta Keywords </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"mk\" value=\"".$metakeywords."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Meta Description </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"md\" value=\"".$metadescription."\" size=30></td></tr>\n";
  echo "<tr><td width=\"100%\" colspan=2><br /><h3>Display Configuration</h3></td></tr>\n";
  echo "<tr><td width=\"40%\"> No. of Entries to Display </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"ndisplay\" value=\"".$ndisplay."\" size=30></td></tr>\n";
  echo "<tr><td width=\"40%\"> Header Image (optional) </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"titleimg\" value=\"".$titleimg."\" size=30><br /> None | [file url] </td></tr>\n";
  echo "<tr><td width=\"40%\"> Main links in sidebar </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"lstatus\" value=\"".$lstatus."\" size=8> On | Off</td></tr>\n";
  echo "<tr><td width=\"40%\"> Pages in sidebar </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"pagestatus\" value=\"".$pagestatus."\" size=8> On | Off</td></tr>\n";
  echo "<tr><td width=\"40%\"> Categories in sidebar </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"catestatus\" value=\"".$catestatus."\" size=8> On | Off</td></tr>\n";
  echo "<tr><td width=\"40%\"> Archives in sidebar </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"archstatus\" value=\"".$archstatus."\" size=8> On | Off</td></tr>\n";
  echo "<tr><td width=\"40%\"> Miscellaneous in sidebar </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"miscstatus\" value=\"".$miscstatus."\" size=8> On | Off</td></tr>\n";
  echo "<tr><td width=\"40%\"> Add-ons in sidebar </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"astatus\" value=\"".$astatus."\" size=8> On | Off</td></tr>\n";
  echo "<tr><td width=\"100%\" colspan=2><br /><h3>Admin Settings</h3></td></tr>\n";
  echo "<tr><td width=\"40%\"> Allow users to add pages </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"upages\" value=\"".$upages."\" size=8> yes | no</td></tr>\n";
  echo "<tr><td width=\"40%\"> Send e-mail alert for new comments </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"emailcomments\" value=\"".$emailcomments."\" size=8> yes | no</td></tr>\n";
  echo "<tr><td width=\"40%\"> Show PHP Errors </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"phperrors\" value=\"".$phperrors."\" size=8> yes | no</td></tr>\n";
  echo "<tr><td width=\"100%\" colspan=2><br /><h3>Comments</h3></td></tr>\n";
  echo "<tr><td width=\"40%\"> Turn on filters </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"filters\" value=\"".$filters."\" size=8> yes | no</td></tr>\n";
  echo "<tr><td width=\"40%\"> Block comments containing the ff. words: </td>\n";
  echo "<td width=\"60%\">\n";
  echo "<input type=\"text\" name=\"badwords\" value=\"".$badwords."\" size=30><br /> (Separate with commas)</td></tr>\n";
  echo "</table>\n\n";
  echo "<br /><br />\n\n";
  echo "<div align=\"center\">\n";
  echo "<button type=\"submit\">Submit</button>&nbsp; \n";
  echo "<button type=\"reset\">Reset</button>\n";
  echo "</div>\n\n";
  echo "</form>\n\n";
 }

 # Now edit the settings and save to database
 if($clean['show'] == "goconfig") {
  $blogtitle = $_POST['blogtitle'];
  $auth = $_POST['author'];
  $user = $_POST['username'];
  $pass = $_POST['password'];
  $email = $_POST['emailadd'];
  $blog = $_POST['blogurl'];
  $tagline = $_POST['tagline'];
  $ndisplay = $_POST['ndisplay'];
  $metakeywords = $_POST['mk'];
  $metadescription = $_POST['md'];
  $lstatus = $_POST['lstatus'];
  $astatus = $_POST['astatus'];
  $archstatus = $_POST['archstatus'];
  $catestatus = $_POST['catestatus'];
  $titleimg = $_POST['titleimg'];
  $upages = $_POST['upages'];
  $pagestatus = $_POST['pagestatus'];
  $miscstatus = $_POST['miscstatus'];
  $timezone = $_POST['timezone'];
  $timeformat = $_POST['timeformat'];
  $emailcomments = $_POST['emailcomments'];
  $phperrors = $_POST['phperrors'];
  $filters = $_POST['filters'];
  $badwords = $_POST['badwords'];

  $message="<?php /*\t".$auth."\t".$blogtitle."\t".$tagline."\t".$user."\t".$pass."\tmailto:".$email."\t".$blog."\t".$ndisplay."\t".$metakeywords."\t".$metadescription."\t".strtolower($lstatus)."\t".strtolower($astatus)."\t".strtolower($archstatus)."\t".strtolower($catestatus)."\t".strtolower($titleimg)."\t".strtolower($upages)."\t".strtolower($pagestatus)."\t".strtolower($miscstatus)."\t".$timezone."\t".$timeformat."\t".$emailcomments."\t".$phperrors."\t".$filters."\t".$badwords."\t*/ ?>";

  $fhandle = fopen("config.php", "w+");        
  fwrite($fhandle,stripslashes($message));
  adminpanel ("The Blog Setting was modified successfully!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel.</a>");
 }

 # Lists comments to be edited or deleted
 if($clean['show'] == "editdeletecomments") {
  echo "<h3>Blog Comments &amp; Commenters</h3>\n";
  for($a=1; $a <= $total; $a++) {
   if(file_exists("$datadirectory/$a.php") AND file_exists("comments/$a.php")) {
    $cfile = file_get_contents("comments/$a.php");
    $bfile = file_get_contents("$datadirectory/$a.php");
    if($cfile !== "" AND $bfile !== "") {
     $bline = explode("\n", $bfile);
     $cline = explode("\n", $cfile);
     for($e = 0; $e < count($cline) - 1; $e++) {
      $com = explode("\t", $cline[$e]);
      $blg = explode("\t", $bline[$e]);
      echo "<h3>$blg[1]</h3>\n";
      echo "<p class=\"highlight\">\n";
      echo "".$com[1]."<br /><br />\n";
      echo "<b>Comment Manager</b>: \n";
      echo "<a href=\"admin.php?show=editcomments&p=".$a."&c=".$e."\">Edit</a> | ";
      echo "<a href=\"admin.php?show=deletecomments&p=".$a."&c=".$e."\">Delete</a><br />\n";
      if($com[4] == "none") {
       echo "<b>Author</b>: $com[2] \n";
      }
      else {
       echo "<b>Author</b>: <a href=\"$com[4]\" target=\"_blank\">".$com[2]."</a> \n";
	  }
	  echo "[ <a href=\"mailto:".$com[5]."\">".$com[5]."</a> ]<br />\n";
	  echo "<b>Date and Time</b>: ".date($timeformat, $com[3])."<br />\n";
      echo "</p>\n";
     }
    }
   }
   else {
    $handlec = fopen("comments/$a.php", "a+");
    $handled = fopen("$datadirectory/$a.php", "a+"); 
    fwrite($handlec, "");  fwrite($handled, "");
   }
  }
  echo "<br /><br /><p><b><u>NOTE</u></b><br />\n";
  echo "If this space is blank, it means you <br />have no comments to edit or delete.</p>\n";
 }

 # Page that displays an editor for the selected comment
 if($clean['show'] == "editcomments") {
  echo "<h3>Blog Comments &amp; Commenters</h3>\n";
  $p = $clean['p'];
  $c = $_GET['c'];
  $cfile = file_get_contents("comments/$p.php");
  $bfile = file_get_contents("$datadirectory/$p.php");
  if($cfile !== "" AND $bfile !== "") {
   $bline = explode("\n", $bfile);
   $cline = explode("\n", $cfile);
   for($e = 0; $e < count($bline) - 1; $e++) {
    $blg = explode("\t", $bline[$e]);
    echo "\n<br /><br /><u>".$blg[1]."</u> - ".$blg[2]."<br /><br />\n";
   }
   $com = explode("\t", $cline[$c]);
   echo "<form method=\"post\" action=\"admin.php?show=gocomments&p=".$p."&c=".$c."\">\n";
   echo "<b>Editor for a comment by ".$com[2]."</b>\n\n";
   echo "<br /><hr size=1 width=400 align=\"left\">\n";
   echo "<textarea name=\"themessage\" cols=50 rows=15>".editor($com[1])."</textarea>\n\n";
   echo "<br /><br /><div align=\"left\">\n";
   echo "<button type=\"submit\">Submit</button>&nbsp;\n";
   echo "<button type=\"reset\">Reset</button>\n</div></form>\n\n";
  }
 }

 # Now edit the comment and save to database
 if($clean['show'] == "gocomments") {
  $p = $clean['p'];
  $c = $_GET['c'];
  $msg = $_POST['themessage'];
  $comfile = file_get_contents("comments/$p.php");
  $cdata = explode("\n", $comfile);
  $linecount = count($cdata) - 1;
  $fulldata = file_get_contents("comments/$p.php");
  $centry = explode("\t", $cdata[$c]);
  $changed = str_replace($centry[1], string_filter_comment($msg), $fulldata);
  $handle = fopen("comments/$p.php", "w+");
  fwrite($handle, $changed);
  adminpanel ("The comments were successfully edited!<br /><br /><a href=\"index.php\"> View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"index.php?show=archives&p=$p\"> View Comments</a><br /><a href=\"admin.php?show=editdeletecomments\"> Edit more Comments</a>");
 }

 # Displays a page for editing the add-ons
 if($clean['show'] == "editads") {
  $ads = file_get_contents("ads.php");
  echo "<h3>Edit Add-Ons ( <a href=\"admin.php?show=deleteads\">Delete all Add-Ons</a> )</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=goads\">\n";
  echo "Extra Stuff (banners, credits, notes, etc.)<br />\n";
  echo "<input type=\"hidden\" name=\"thetitle\" value=\"1\">\n";
  echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\">".editor($ads)."</textarea>\n\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">Submit</button>&nbsp;\n";
  echo "<button type=\"reset\">Reset</button>\n</div></form>\n\n";
 }

 # Now edit the ads and save to database
 if($clean['show'] == "goads") {
  $a=$_POST['themessage'];
  $handle = fopen("ads.php", "w+");
  fwrite($handle, newlines($a));
  adminpanel ("The blog advertisements were successfully edited!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel.</a>");
 }

 # Displays a page for choosing themes
 if($clean['show'] == "themes") {
  $dir = "themes/";
  echo "<h3>OZJournal Themes</h3>\n";
  echo "Choose a theme to select, edit or delete:\n";
  echo "<ol type=\"1\">\n";
  if(is_dir($dir)) {
   if($dh = opendir($dir)) {
    while(($file = readdir($dh)) !== false) {
     if(filetype($dir . $file) != "file") {
      if($file !== "." AND $file !== "..") {
       echo "<li>\n";
       echo "<a href=\"admin.php?show=gotheme&id=$file\">Use</a> | ";
       echo "<a href=\"admin.php?show=theme_editor&id=$file\">Edit</a> | ";
       $tc = file_get_contents("themes/index.php");
       $current_theme = explode("\t", $tc);
       if($file == $current_theme[1]) {
        echo "Delete <b>$file</b>";
       }
	   else {
        echo "<a href=\"admin.php?show=delete_theme&id=$file\">Delete</a> <b>$file</b>";
	   }
       if($file == $current_theme[1]) {
        echo " (selected) </li>";
       }
      }
     }
    }
    closedir($dh);
   }
  }
  echo "</ol><br />\n";
  echo "<fieldset><a href=\"admin.php?show=new_theme\">Create a New Theme</a></fieldset>\n";
 }

 # Confirms the selected theme
 if($clean['show'] == "gotheme") {
  $chosen = $clean['id'];
  $handle = fopen("themes/index.php", "w+");
  $writetheme = "<?php /*\t".$chosen."\t*/ ?>\n";
  fwrite($handle, stripslashes($writetheme));
  adminpanel ("You have successfully changed the theme for this blog!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel.</a>");
 }

 # Displays a page for editing the selected theme
 if($clean['show'] == "theme_editor") {
  $theme = $clean['id'];
  $ozheader = file_get_contents("themes/$theme/header.php");
  $ozfooter = file_get_contents("themes/$theme/footer.php");
  $ozadvanced = file_get_contents("themes/$theme/advanced.php");
  echo "<h3>Theme Editor</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=go_theme_editor&id=$theme\">\n";
  echo "Header<br /><br />\n\n";
  echo "<textarea name=\"ozheader\" cols=50 rows=15 wrap=\"off\" class=\"mceNoEditor\">".stripslashes($ozheader)."</textarea><br /><br />\n";
  echo "Footer<br /><br />\n\n";
  echo "<textarea name=\"ozfooter\" cols=50 rows=15 wrap=\"off\" class=\"mceNoEditor\">".stripslashes($ozfooter)."</textarea><br /><br />\n";
  echo "Advanced. For users who have knowledge in PHP.<br /><br />\n\n";
  echo "<textarea name=\"ozadvanced\" cols=50 rows=15 wrap=\"off\" class=\"mceNoEditor\">".$ozadvanced."</textarea>\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">Submit</button>&nbsp;\n";
  echo "<button type=\"reset\">Reset</button>\n\n";                                
  echo "<input type=\"hidden\" name=\"theme\" value=\"".$theme."\">\n";
  echo "</div></form>\n";
 }

 # Edit the theme and save to database
 if($clean['show'] == "go_theme_editor") {
  $theme = $clean['id'];
  $ozheader1 = $_POST['ozheader'];
  $ozfooter1 = $_POST['ozfooter'];
  $ozadvanced1 = $_POST['ozadvanced'];
  if($ozheader !== "" && $ozfooter !== "" && $ozadvanced !== "") {
   $ozhhandle = fopen("themes/$theme/header.php", "w+");
   $ozfhandle = fopen("themes/$theme/footer.php", "w+");
   $ozahandle = fopen("themes/$theme/advanced.php", "w+");
   fwrite($ozhhandle, stripslashes($ozheader1));
   fwrite($ozfhandle, stripslashes($ozfooter1));
   fwrite($ozahandle, stripslashes($ozadvanced1));
   adminpanel ("The theme was successfully edited!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel.</a>");
  }
  else {
   adminpanel ("The content cannot be blank.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Page for creating a new theme out of the default
 if($clean['show'] == "new_theme") {
  $ozheader = file_get_contents("themes/default/header.php");
  $ozfooter = file_get_contents("themes/default/footer.php");
  $ozadvanced = file_get_contents("themes/default/advanced.php");
  echo "<h3>Theme Editor</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=go_new_theme\">\n";
  echo "Theme Name: <input type=\"text\" name=\"themename\" size=\"30\"><br /><br />";
  echo "Header<br /><br />\n\n";
  echo "<textarea name=\"ozheader\" cols=50 rows=15 wrap=\"off\" class=\"mceNoEditor\">".stripslashes($ozheader)."</textarea><br /><br />\n";
  echo "Footer<br /><br />\n\n";
  echo "<textarea name=\"ozfooter\" cols=50 rows=15 wrap=\"off\" class=\"mceNoEditor\">".stripslashes($ozfooter)."</textarea><br /><br />\n";
  echo "Advanced. For users who have knowledge in PHP.<br /><br />\n\n";
  echo "<textarea name=\"ozadvanced\" cols=50 rows=15 wrap=\"off\" class=\"mceNoEditor\">".$ozadvanced."</textarea>\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">Submit</button>&nbsp;\n";
  echo "<button type=\"reset\">Reset</button>\n\n";                                
  echo "<input type=\"hidden\" name=\"theme\" value=\"".$theme."\">\n";
  echo "</div></form>\n";
 }

 # Save the new theme to database
 if($clean['show'] == "go_new_theme") {
  $themename = $_POST['themename'];
  $ozheader1 = $_POST['ozheader'];
  $ozfooter1 = $_POST['ozfooter'];
  $ozadvanced1 = $_POST['ozadvanced'];
  if($themename !== "" && $ozheader1 !== "" && $ozfooter1 !== "" && $ozadvanced1 !== "") {
   if(mkdir("themes/$themename")) {
    chmod("themes/$themename", 0777);
   }
   $ozhhandle = fopen("themes/$themename/header.php", "w+");
   $ozfhandle = fopen("themes/$themename/footer.php", "w+");
   $ozahandle = fopen("themes/$themename/advanced.php", "w+");
   fwrite($ozhhandle, stripslashes($ozheader1));
   fwrite($ozfhandle, stripslashes($ozfooter1));
   fwrite($ozahandle, stripslashes($ozadvanced1));
   adminpanel ("The theme was successfully created!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel.</a>");
  }
  else {
   adminpanel ("The content cannot be blank.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Deletes the theme
 if($clean['show'] == "delete_theme") {
  $theme = $clean['id'];
  delete_theme("themes/$theme/");
  rmdir("themes/$theme");
  adminpanel ("The theme was successfully deleted!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel.</a><br /><a href=\"admin.php?show=themes\">Delete more themes.</a>");
 }

 # Confirm post deletion
 if($clean['show'] == "displaydelete") {
  $journal = $clean['n'];
  deletefile ($journal, $datadirectory, $total);
  deletefile ($journal, "comments", $total);
  adminpanel ("The journal entry has been successfully deleted! <br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editjournals\"> Delete more Journals</a>.");
 }

 # Confirm page deletion
 if($clean['show'] == "displaydeletepages") {
  $journal = $clean['f'];
  deletefile ($journal, "pages", countfiles("pages"));
  adminpanel ("The static page has been successfully deleted! <br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editpages\"> Delete more Pages</a>.");
 }

 # Confirm comment deletion
 if($clean['show'] == "deletecomments") {
  $p = $clean['p'];
  $c = $_GET['c'];
  $msg = $_POST['themessage'];
  $comfile = file_get_contents("comments/$p.php");
  $cdata = explode("\n", $comfile);
  $linecount = count($cdata);
  $b = $c - 1;
  $fulldata = file_get_contents("comments/$p.php");
  $changed = str_replace("$cdata[$c]\n", "", $fulldata);
  $handle = fopen("comments/$p.php", "w+");
  fwrite($handle, stripslashes($changed));
  adminpanel ("The comments were successfully deleted!<br /><br /><a href=\"index.php\"> View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editdeletecomments\"> Delete more Comments</a>");
 }

 # Confirm category deletion
 if($clean['show'] == "displaydeletecategories") {
  $c = $_GET['c'];
  $cfile = file_get_contents("categories.php");
  $changed = str_replace("<?php /*\t$c\t*/ ?>\n", "", $cfile); 
  $handle = fopen("categories.php", "w+");
  fwrite($handle, stripslashes($changed));
  adminpanel ("The category was successfully deleted!<br /><br /><a href=\"index.php\"> View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editcategories\"> Delete more Categories</a>");
 }

 # Confirm archive deletion
 if($clean['show'] == "deletearchives") {
  $d = $_GET['d'];
  @unlink("archives/$d.php");
  for($k = 1; $total >= $k; $k++) {
   $maincont = file_get_contents("$datadirectory/$k.php");
   $ent = explode("\t", $maincont);
   $pmonth = date("F", $ent[2]);
   $pyear = date("Y", $ent[2]);
   $dat = explode("-", $d);
   $amonth = $dat[1];
   $ayear = $dat[0];
   if($pmonth==$amonth AND $pyear=$ayear) {
    @unlink("$datadirectory/$k.php");
    @unlink("comments/$k.php");
   }
  }
  adminpanel ("The archive date has been successully deleted! <br /><br /><a href=\"index.php\"> View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editarchives\"> Edit more Archives</a>");
 }

 # Confirm add-on deletion
 if($clean['show'] == "deleteads") {
  deletecontents ("ads.php");
  adminpanel ("All blog advertisements have been successully deleted! <br /><br /><a href=\"index.php\"> View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a>");
 }

 # Confirm counter refresh
 if($clean['show'] == "refreshhits") {
  deletecontents ("counter.php");
  adminpanel ("All blog hits have been successully deleted! <br /><br /><a href=\"index.php\"> View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a>");
 }

 # Shows the blog site statistics
 if($clean['show'] == "statistics") {
  echo "<h3>Blog Statistics</h3>\n";
  echo "<p>This blog has a total of <b>".$total." journal(s)</b> and \n";
  echo "<b>".countfiles('pages')." static page(s)</b>. <br /> \n";
  if(file_exists("counter.php")) {
   $counterfile = file_get_contents("counter.php");
   $counterdata = explode("|", $counterfile);
   $countermagic = count($counterdata) - 1;
   echo "It has also registered a total of <b>".$countermagic." page hit(s)</b>.</p>\n";
  }
  echo "<p><a href=\"admin.php?show=refreshhits\">Refresh Blog Hits</a></p>\n\n";
 }

 # Show the user configuration
 if($clean['show'] == "userconfig") {
  if(file_exists("usersdb.php")) {
   $usersdb = file_get_contents("usersdb.php");
   $eachuser = explode("\n", $usersdb);
   $i = count($eachuser) - 1;
   echo "<h3>User Configuration</h3><b>The Writers/Contributors of this Blog</b><ul type\"square\">\n";
   for($r=0; $r < $i; $r++) {
    $userdata = explode("\t", $eachuser[$r]);
    if($userdata[1] !== "") {
     echo "<form method=\"post\" action=\"admin.php?show=edituser\">\n";
     echo "<li><input type=\"hidden\" name=\"id\" value=\"$r\">\n";
     echo "<input type=\"text\" name=\"newname\" value=\"$userdata[2]\" size=\"15\"> \n";
     echo "<button type=\"submit\">Edit</button> | \n";
     echo "<a href=\"admin.php?show=deleteuser&id=$r\">Delete</a></form>\n";
    }
   }
   echo "</ul>\n";
  }
  else {
   adminpanel ("The <code>usersdb</code> file does not exist.");
  }
  echo "<br /><h3>Add A User</b></h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=addposter\">\n";
  echo "<table border=\"0\" width=\"100%\">";
  echo "<tr><td width=\"30%\">Username</td>\n";
  echo "<td width=\"70%\"><input type=\"text\" name=\"uname\" /></td></tr>\n";
  echo "<tr><td width=\"30%\">E-mail Address</td>\n";
  echo "<td width=\"70%\"><input type=\"text\" name=\"uemail\" /></td></tr>\n";
  echo "<tr><td width=\"30%\">Password</td>\n";
  echo "<td width=\"70%\"><input type=\"password\" name=\"upass\" /></td></tr>\n";
  echo "</table><br />\n";
  echo "<button type=\"submit\">Submit</button>&nbsp;\n";
  echo "<button type=\"reset\">Reset</button>\n";
  echo "</form><br /><br />\n\n";
 }

 # Confirm registration of new users
 if($clean['show'] == "addposter") {
  $uname = $_POST['uname'];
  $uemail = $_POST['uemail'];
  $upass = md5($_POST['upass']);
  $userdatabase = file("usersdb.php");
  $userlines = count($userdatabase) + 1;
  $addthis = "<?php /*\t".$userlines."\t".$uname."\t".$uemail."\t".$upass."\t*/ ?>\n";
  $ah = fopen("usersdb.php", "a+");
  fwrite($ah, $addthis);
  adminpanel ("You have successfully added a user. <br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=userconfig\">Back to User Configuration</a> ");
 }

 # Edit user information and save to database
 if($clean['show'] == "edituser") {
  $id = $_POST['id'];
  $newn = $_POST['newname'];
  $usersdb = file_get_contents("usersdb.php");
  $userline = explode("\n", $usersdb);
  $userdata = explode("\t", $userline[$id]);
  $changed = str_replace($userdata[2], $newn, $usersdb);
  $handleuser = fopen("usersdb.php", "w+");
  fwrite($handleuser, stripslashes($changed));
  adminpanel("You have successfully edited the user's data.<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=userconfig\">Back to User Configuration</a>");
 }

 # Confirm deletion of user
 if($clean['show'] == "deleteuser") {
  $id = $clean['id'];
  $usersdb = file_get_contents("usersdb.php");
  $userline = explode("\n", $usersdb);
  $userdata = explode("\t", $userline[$id]);
  $changed = str_replace("$userdata[0]\t$userdata[1]\t$userdata[2]\t$userdata[3]\t$userdata[4]\t$userdata[5]\n", "", $usersdb);
  $handleuser = fopen("usersdb.php", "w+");
  fwrite($handleuser, stripslashes($changed));
  adminpanel("You have successfully deleted the user's account.<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=userconfig\">Back to User Configuration</a>");
 }
}

if($_SESSION['mode'] == "user") {

 # Displays the control panel page
 if($clean['show'] == "" OR $clean['show'] == "main") {
  echo "<h3>Welcome to the user's panel! &Uuml;</h3>\n";
  echo "<p><b>Greetings, <em>".$_SESSION['user']."</em>!</b><br /> \n";
  echo "Please select a link below so you can start managing your blog.</p>\n\n";
  echo "<table cellspacing=0 cellpadding=8 border=0 width=\"100%\">\n";
  echo "<tr><td width=\"10%\"><b>Write</b>: </td>\n";
  if($upages == "yes") {
   echo "<td width=\"90%\"><a href=\"admin.php?show=add\">Journals</a> \n"; 
   echo " - <a href=\"admin.php?show=addpages\">Pages</a></td></tr>\n";
  }
  else {
   echo "<td width=\"90%\"><a href=\"admin.php?show=add\">Journals</a></td</tr> \n";
  }
  echo "<tr><td width=\"10%\"><b>Edit/Delete</b>: </td>\n";
  if($upages == "yes") {
   echo "<td width=\"90%\"><a href=\"admin.php?show=editjournals\">Journals</a> \n";
   echo " - <a href=\"admin.php?show=editpages\">Pages</a></td></tr>\n";
  }
  else {
   echo "<td width=\"90%\"><a href=\"admin.php?show=editjournals\">Journals</a></td</tr> \n";
  }
  echo "<tr><td width=\"10%\"><b>View</b>: </td>\n";
  echo "<td width=\"90%\"><a href=\"admin.php?show=statistics\">Blog Statistics</a></td></tr>\n";
  echo "<tr><td width=\"10%\"><b>ozjournals</b>: </td>\n";
  echo "<td width=\"90%\"><a href=\"readme.php\">Readme</a> \n";
  echo " - <a href=\"license.txt\">License</a> \n";
  echo " - <a href=\"https://sites.google.com/site/onlinezonejournals\" target=\"_blank\">Software Updates</a></td></tr>\n";
  echo "</table><br /><br />\n\n";
  echo "<br /><p>Have fun using OZJournals! <br /> For more information and downloads, <br /> please visit our site at \n";
  echo "<a href=\"https://sites.google.com/site/onlinezonejournals\" target=\"_blank\">https://sites.google.com/site/onlinezonejournals</a>.\n";
  echo "</p><br />\n\n";
 }

 # Page for adding posts
 if($clean['show'] == "add") {
  echo "<h3>Add a Journal Entry</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=post\">\n";
  echo "<b>Status:</b> <input type=\"radio\" name=\"saveas\" value=\"draft\"> Draft | \n";
  echo "<input type=\"radio\" name=\"saveas\" value=\"published\" checked> Published &nbsp;&nbsp;&nbsp;\n\n";
  echo "<b>Category:</b> <select name=\"category\"><option value=\"No Category\" selected>No Category</option>\n";
  if(file_exists("categories.php")) {
   $categoryfile = file_get_contents("categories.php");
   $eachcategory = explode("\n", $categoryfile);
   $allcategories = count($eachcategory) - 1;
   for($c = 0; $c < $allcategories; $c++) {
    $realc=explode("\t", $eachcategory[$c]);
    echo "<option value=\"".tospace($realc[1])."\">".tospace($realc[1])."</option>\n";
   }
  }
  else {
   echo "<li>The <code>categories</code> file does not exist.";
  }
  echo "</select><br /><br />\n\n";        
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\" checked> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\"> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" /> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
  echo "<b>TITLE:</b> <input type=\"text\" name=\"thetitle\" size=40 maxlength=50><br /> \n";
  echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\"></textarea>\n\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "<input type=\"hidden\" name=\"hidden\" value=\"".countfiles('posts')."\">\n";
  echo "</div></form><br />\n";
 }

 # Now add the post to database
 if($clean['show'] == "post") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
  $t = $_POST["thetitle"];
  $m = $_POST["themessage"];
  $c = $_POST["category"];
  $s = $_POST["saveas"];
  $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$_SESSION['user']."\t\t".$c."\t".$s."\t*/ ?>";
  $hidden=$_POST['hidden'];
  $e=$hidden +1;
  $month = date_and_time($timezone, "F", "", "", "", "", "", "");
  $year = date_and_time($timezone, "Y", "", "", "", "", "", "");
  if($t !== "" && $m !== "") {
   if(!file_exists("archives/$year-$month.php")) {
    $yh = fopen("archives/$year-$month.php", "x");
   }
   $handle=fopen("$datadirectory/$e.php" , "w+");
   fwrite($handle, newlines($message));
   $handler=fopen("comments/$e.php" , "w+");
   fwrite($handler, "");
   adminpanel ("You have successfully added a journal entry!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=add\">Add another entry</a>");
  }
  else {
   adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this post.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Page for adding new front-end pages
 if($upages=="yes") {
  if($clean['show'] == "addpages") {
   echo "<h3>Add a Page</h3>\n";
   echo "<form method=\"post\" action=\"admin.php?show=postpages\">\n";
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\" checked> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\"> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" /> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
   echo "<b>TITLE:</b> <input type=\"text\" name=\"thetitle\" size=40 maxlength=50><br />\n\n";        
   echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\"></textarea>\n\n";
   echo "<br /><br /><div align=\"left\">\n";
   echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
   echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
   echo "<input type=\"hidden\" name=\"hidden\" value=\"".countfiles('pages')."\">\n";
   echo "</div></form><br />\n";
  }

 # Now add those new pages to the database
  if($clean['show'] == "postpages") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
   $t=$_POST["thetitle"];
   $m=$_POST["themessage"];
   $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$_SESSION["user"]."\t\t*/ ?>";
   $hidden=$_POST['hidden'];
   $e=$hidden +1;
    if($t !== "" && $m !== "") {
    $handle=fopen("pages/$e.php" , "w+");
    fwrite($handle, newlines($message));
    adminpanel ("You have successfully added a page!<br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\">Back to Admin Control Panel</a><br /><a href=\"admin.php?show=addpages\">Add another page</a>");
   }
   else {
    adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this page.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
   }
  }
 }
 
 # Page for listing the posts to be edited or deleted
 if($clean['show'] == "editjournals") {
  if($total >0) {
   echo "<h3>Edit Journals</h3>\n";
   echo "<p>Please select a journal to edit or delete.<p>\n\n";
   $total = countfiles($datadirectory);
   $p=$clean['p'];
   if($p == "") {
    echo editdisplayall ($datadirectory, $total, $_SESSION['user']);
   }
  }
  else {
   echo adminpanel ("No journals yet!<br /><br />");
  }
 }

 # Page for editing the post
 if($clean['show'] == "displayedit") {
  $jnumber = $clean['n'];
  $file = file_get_contents("$datadirectory/$jnumber.php");
  $each = explode("\t",$file);
  echo "<h3>Editor for ".$each[1]." content</h3>\n";
  echo "<form method=\"post\" action=\"admin.php?show=goedit&n=".$jnumber."\">\n";
  echo "<b>Status:</b> <input type=\"radio\" name=\"saveas\" value=\"draft\" ";
  if($each[7] == "draft") {
   echo " checked";
  }
  echo "> Draft | \n";
  echo "<input type=\"radio\" name=\"saveas\" value=\"published\" ";
  if($each[7] == "published") {
   echo " checked";
  }
  echo "> Published &nbsp;&nbsp;&nbsp;\n\n";
  echo "<b>Category:</b> <select name=\"category\"><option value=\"No Category\" ";
  if($each[6] == "No Category") {
   echo " selected";
  }
  echo ">No Category</option>\n";
  if(file_exists("categories.php")) {
   $categoryfile = file_get_contents("categories.php");
   $eachcategory = explode("\n", $categoryfile);
   $allcategories = count($eachcategory) - 1;
   for($c = 0; $c < $allcategories; $c++) {
    $eachc = explode("\n", $categoryfile);
    $eachrealc = explode("\t", $eachc[$c]);	   
    echo "<option value=\"".tospace($eachrealc[1])."\" ";
    if($each[6] == tospace($eachrealc[1])) {
	 echo " selected";
	}
    echo ">".tospace($eachrealc[1])."</option>\n";
   }
  }
  else {
   echo "<li>The <code>categories</code> file does not exist.";
  }
  echo "</select><br /><br />\n\n";        
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\"> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\" checked> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" value=\"";
  echo date("m-d-Y-H-i", $each[2]);
  echo "\"/> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
  echo "<b>TITLE:</b> <input type=\"text\" name=\"thetitle\" value=\"$each[1]\" size=40 maxlength=50><br />\n";        
  echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\">".editor($each[3])."</textarea>\n\n";
  echo "<br /><br /><div align=\"left\">\n";
  echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
  echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
  echo "</div></form><br />\n";
 }

 # Now edit the post and save to database
 if($clean['show'] == "goedit") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
  $t = $_POST['thetitle'];
  $m = $_POST['themessage']; 
  $w = $clean['n'];
  $c = $_POST['category'];
  $s = $_POST['saveas'];
  $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$_SESSION["user"]."\t\t".$c."\t".$s."\t*/ ?>";
  $month = date_and_time($timezone, "F", "", "", "", "", "", "");
  $year =  date_and_time($timezone, "Y", "", "", "", "", "", "");
  if($t !== "" && $m !== "") {
   if(!file_exists("archives/$year-$month.php")) {
    $yh = fopen("archives/$year-$month.php", "x");
   }
   $fhandle = fopen("$datadirectory/$w.php", "w+");
   fwrite($fhandle,newlines($message));
   adminpanel ("The journal was modified successfully!<br /><br /><a href=\"index.php?show=archives&p=".$w."\"> View my Journal</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editjournals\"> Edit more Journals</a>.");
  }
  else {
   adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this post.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
  }
 }

 # Displays a list of the pages to be edited or deleted
 if($upages=="yes") {
  if($clean['show'] == "editpages") {
   $totalpages = countfiles("pages");
   if($totalpages >0) {
    echo "<h3>Edit Pages</h3>\n";
    echo "<p>Please select a static page to edit or delete.<p>\n\n";
    echo "<ol type=\"1\">\n";
    $dir = "pages/";
    if(is_dir($dir)) {
     if($dh = opendir($dir)) {
      while(($file = readdir($dh)) !== false) {
       if($file !== "." AND $file !== "..") {
        if(filetype($dir.$file) !== "dir") {
         $pc = file_get_contents($dir.$file);
         $eachpc = explode("\t", $pc);
         echo "<li> <a href=\"admin.php?show=displayeditpages&f=".substr($file, 0, 1)."\">Edit</a> | \n";
         echo "<a href=\"admin.php?show=displaydeletepages&f=".substr($file, 0, 1)."\">Delete</a> \n";
         echo "<b>".$eachpc[1]."</b>\n\n";
        }
       }
      }
      closedir($dh);
      echo "</ol><br />\n\n";
     }
    }
   }
   else {
    echo adminpanel ("No pages yet! <br /><br />");
   }
  }

 # Displays a page for editing the selected pages to be edited... o_0 The redundancy of it all...
  if($clean['show'] == "displayeditpages") {
   $pn = $clean['f'];
   $filepc = file_get_contents("pages/$pn.php");
   $datapc = explode("\t",$filepc);
   echo "<h3>Editor for ".$datapc[1]." content</h3>\n";
   echo "<form method=\"post\" action=\"admin.php?show=goeditpages&n=".$pn."\">\n";
  echo "<b>Date:</b><br /> <input type=\"radio\" name=\"date_option\" value=\"now\"> Now <br /> \n";
  echo "<input type=\"radio\" name=\"date_option\" value=\"set\" checked> Set: \n\n";
  echo "<input type=\"text\" name=\"set_date\" value=\"";
  echo date("m-d-Y-H-i", $datapc[2]);
  echo "\"/> \n";
  echo "<code>(MM-DD-YYYY-Hour-Minute)<br />\n\n";
  echo "&nbsp;&nbsp;&nbsp;&nbsp;Ex. 12-13-2009-21-53 </code><br /><br />\n\n";
   echo "<b>TITLE:</b> <input type=\"text\" name=\"thetitle\" value=\"$datapc[1]\" size=40 maxlength=50><br />\n\n";        
   echo "<textarea name=\"themessage\" cols=\"50\" rows=\"15\">".editor($datapc[3])."</textarea>\n\n";
   echo "<br /><br /><div align=\"left\">\n";
   echo "<button type=\"submit\">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>&nbsp;\n";
   echo "<button type=\"reset\">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>\n";
   echo "</div>\n";
   echo "</form><br />\n";
  }

 # Now edit the page and save to database
  if($clean['show'] == "goeditpages") {
  $date_option = $_POST["date_option"];
  $set_date = explode("-", $_POST["set_date"]);
  $set_date_month = $set_date[0];
  $set_date_day = $set_date[1];
  $set_date_year = $set_date[2];
  $set_date_hour = $set_date[3];
  $set_date_minute = $set_date[4];
  if($date_option=="set") {
   $d = mktime($set_date_hour, $set_date_minute, 0, $set_date_month, $set_date_day, $set_date_year);
   }
  elseif($date_option=="now") {
   $d = date_and_time($timezone, "", "", "", "", "", "", "");
  }
   $t = $_POST['thetitle'];
   $m = $_POST['themessage']; 
   $w = $clean['n'];
   $message="<?php /*\t".$t."\t".$d."\t".$m."\t".$_SESSION["user"]."\t\t*/ ?>";
   if($t !== "" && $m !== "") {
    $fhandle = fopen("pages/$w.php", "w+");        
    fwrite($fhandle,newlines($message));
    adminpanel ("The page was modified successfully!<br /><br /><a href=\"index.php?show=pages&f=".$w."\"> View Page</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editpages\"> Edit more Pages</a>.");
   }
   else {
    adminpanel ("Please include a <b>TITLE</b> and <b>CONTENT</b> for this page.<br /><a href=\"javascript: history.go(-1)\">Go Back</a>");
   }
  }

 # Confirm page deletion
  if($clean['show'] == "displaydeletepages") {
   $journal = $clean['f'];
   deletefile ($journal, "pages", countfiles("pages"));
   adminpanel ("The static page has been successfully deleted! <br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editpages\"> Delete more Pages</a>.");
  }
 }
 
 # Confirm post deletion
 if($clean['show'] == "displaydelete") {
  $journal = $clean['n'];
  deletefile ($journal, $datadirectory, $total);
  deletefile ($journal, "comments", $total);
  adminpanel ("The journal entry has been successfully deleted! <br /><br /><a href=\"index.php\">View Blog</a><br /><a href=\"admin.php\"> Back to Admin Control Panel</a><br /><a href=\"admin.php?show=editjournals\"> Delete more Journals</a>.");
 }

 # Shows the blog site statistics
 if($clean['show'] == "statistics") {
  echo "<h3>Blog Statistics</h3>\n";
  echo "<p>This blog has a total of <b>".$total." journal(s)</b> and \n";
  echo "<b>".countfiles("pages")." static page(s)</b>. <br /> \n";
  if(file_exists("counter.php")) {
   $counterfile = file_get_contents("counter.php");
   $counterdata = explode("|", $counterfile);
   $countermagic = count($counterdata) - 1;
   echo "It has also registered a total of <b>".$countermagic." page hit(s)</b>.</p>\n";
  }
 }

}

# Footer
$footer = implode(" ", file("themes/$oktheme/footer.php"));
$footer = str_replace("{blogtitle}", $blogtitle, $footer);
$header = str_replace("{subtitle}", subtitle($clean["show"], $clean["p"], $clean["f"]), $header);
$footer = str_replace("{metakeywords}", $metakeywords, $footer);
$footer = str_replace("{metadescription}", $metadescription, $footer);
$footer = str_replace("{author}", $auth, $footer);
$footer = str_replace("{header}", $blogheader, $footer);
$footer = str_replace("{mainlinks}", $mainlinks, $footer);
$footer = str_replace("{add-ons}", "", $footer);
$footer = str_replace("{search}", "", $footer);
$footer = str_replace("{categories}", "", $footer);
$footer = str_replace("{archives}", "", $footer);
$footer = str_replace("{miscellaneous}", "", $footer);
$footer = str_replace("{adminmenu}", $adminmenu, $footer);
$footer = str_replace("{usermenu}", $usermenu, $footer);
$footer = str_replace("{footer}", $blogfooter, $footer);
echo $footer;

?>