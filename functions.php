<?php 

/********************************************************************************************************

   OZJournals Version 3.2 released by Online Zone <https://sites.google.com/site/onlinezonejournals>
   Copyright (C) 2006 -2011 Elaine Aquino <elaineaqs@gmail.com>

   This program is free software; you can redistribute it and/or modify it 
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2 of the License, or (at your option) 
   any later version.

********************************************************************************************************/

# See if it's already installed
if(file_exists("config.php")) {
 $config = file_get_contents("config.php");
 $c = explode("\t", $config);

 $auth = $c[1];
 $blogtitle = $c[2];
 $tagline = $c[3];
 $user = $c[4];
 $pass = $c[5];
 $email = $c[6];
 $blog = $c[7];
 $ndisplay = $c[8];
 $metakeywords = $c[9];
 $metadescription = $c[10];
 $lstatus = $c[11];
 $astatus = $c[12];
 $archstatus = $c[13];
 $catestatus = $c[14];
 $titleimg = $c[15];
 $upages = $c[16];
 $pagestatus = $c[17];
 $miscstatus = $c[18];
 $timezone = $c[19]; 
 $timeformat = $c[20]; 
 $emailcomments = $c[21];
 $phperrors = $c[22];
 $filters = $c[23];
 $badwords = $c[24];
}
# Install if config.php file does not exist
else {
 header("Location: setup.php");
}

# All-around variables
# Name of the directory where the posts are stored
$datadirectory = "posts";

# Number of posts to show in a page
$displaynumber = $ndisplay;

# Total number of files from the posts directory
$total = countfiles($datadirectory);

# Error Reporting
ini_set("error_reporting", "E_ALL"); //Report all errors
if($phperrors!=="yes") {
ini_set("display_errors", "Off"); //Turn off PHP Errors
}
else {
ini_set("display_errors", "On"); //Turn on PHP Errors
}

# Start the functions
# Count how many files there in a directory
function countfiles($dirname) {
 $result = 0;
 clearstatcache();
 if(is_dir($dirname)) {
  if(($dir = @opendir($dirname))!==false) {
   while($filename = readdir($dir)) {
    if($filename != '..' && $filename != '.') {
	 $result++;
	}
   }
   @closedir($dir);
  }
 }
 else {
  $result = 0;
 }
 return $result;
}

# Display function if the total number of posts is more than the number of posts to be shown in a page
# Figure that out... o_0
function display($number, $dir, $count, $startfrom) {
 global $timeformat, $lang;
 $total = $count;
 if($total > $number) {
  $dthis = $startfrom - $number;
  for($n=$startfrom; $n > $dthis; $n--) {
   if(file_exists("$dir/$n.php") AND file_exists("comments/$n.php")) {
    $data=file_get_contents("$dir/$n.php");
    if($data !== "") {
     $line = explode("\t", $data); 
     $i=count($line); 
     $cfile = file_get_contents("comments/$n.php");
     if($cfile !== "") {
      $cexplode = explode("\n", $cfile);
      $n1 = count($cexplode) - 1;
     }
	 else {
	  $n1 = 0; 
	 }
     switch($n1) {
      case 0:
      $snoc = "$lang[55]"; break;
      case 1:
      $snoc = "$lang[56]"; break;
      default:
      $snoc = "$n1 $lang[57]"; break;
	 }
     if($line[7] == "published") {
      echo "<h3>".$line[1]."</h3>\n";
      echo "<h5>$lang[12] ";
      echo "<a href=\"".$line[5]."\">".$line[4]."</a> $lang[11] ".date($timeformat, $line[2])." </h5> \n\n";
      echo "<p>".stripslashes($line[3])."</p><br />\n\n";
      echo "<a href=\"index.php?show=archives&p=".$n."\">".$snoc."</a> | ";
      if(file_get_contents("categories.php") == "") {
	   echo ""; 
	  }
      else {
	   echo "<a href=\"index.php?show=archives&c=".$line[6]."\">".$line[6]."</a> | "; 
	  }
      echo "<a href=\"index.php?show=printpreview&id=".$n."\">$lang[58]</a><br /><br /><br />\n\n";
     }
     else { }
    }
   }
  }
  
  $something = $startfrom - $number;
  $prev = $startfrom + $number;
  
  echo "<p align=\"center\" class=\"highlight\">";
  if($startfrom < $total && $something < 1 ) {
   echo "<a href=\"index.php?show=main&pn=$prev\">$lang[59]</a> | $lang[60]\n"; 
  }
  if($something >= 1 && $startfrom >= $total) {
   echo "$lang[59] | <a href=\"index.php?show=main&pn=$dthis\">$lang[60]</a>\n";	
  }
  if($startfrom < $total && $something >= 1) { 
   echo "<a href=\"index.php?show=main&pn=$prev\">$lang[59]</a> \n";
   echo " | <a href=\"index.php?show=main&pn=$dthis\">$lang[60]</a>"; 
  }
  echo "</p>";
 }
 else {
  for($n=$total; $n > 0; $n--) {
   $data=file_get_contents("$dir/$n.php");
   if($data !== "") {
    $line = explode("\t", $data); 
    $i=count($line); 
    $cfile = file_get_contents("comments/$n.php");
    if($cfile !== "") {
     $cexplode = explode("\n", $cfile);
     $n1 = count($cexplode) - 1;
    }
	else {
	 $n1 = 0;
	}
    switch($n1) {
    case 0:
	$snoc = "$lang[55]"; break;
	case 1:
	$snoc = "$lang[56]"; break;
	default:
	$snoc = "$n1 $lang[57]"; break;
    }
    if($line[7] == "published") {
     echo "<h3>".$line[1]."</h3>\n";
     echo "<h5>$lang[12] ";
     echo "<a href=\"".$line[5]."\">".$line[4]."</a> $lang[11] ".date($timeformat, $line[2])." </h5> \n\n";
     echo "<p>".stripslashes($line[3])."</p><br />\n\n";
     echo "<a href=\"index.php?show=archives&p=".$n."\">".$snoc."</a> | ";
     if(file_get_contents("categories.php") == "") {
	  echo "";
	 }
     else {
	  echo "<a href=\"index.php?show=archives&c=".$line[6]."\">".$line[6]."</a> | ";
	 }
     echo "<a href=\"index.php?show=printpreview&id=".$n."\">$lang[58]</a><br /><br /><br />\n\n";
    }
   }
   else { }
  }
 }
}

# Displays individual posts in a page with comment form
function displayeach($dir, $f) {
 global $timeformat, $lang;
 if(file_exists("$dir/$f.php")) {
  $n = $_GET["p"];
  $data = file_get_contents("$dir/$f.php");
  if($data !== "") {
   $line = explode("\t", $data); 
   $i=count($line); 
   $cfile = file_get_contents("comments/$f.php");
   if($cfile !== "") {
    $cexplode = explode("\n", $cfile);
    $n1 = count($cexplode) - 1;
   }
   else {
    $n1 = 0;
   }
   switch($n1) {
    case 0:
	 $snoc = "$lang[55]";
	 break;
	case 1:
	 $snoc = "$lang[56]";
	 break;
    default:
     $snoc = "$n1 $lang[57]";
	 break;
   }
   if($line[7] == "published") {
    echo "<h3>".$line[1]."</h3>\n";
    echo "<h5>$lang[12] ";
    echo "<a href=\"".$line[5]."\">".$line[4]."</a> $lang[11] ".date($timeformat, $line[2])." </h5> \n\n";
    echo "<p>".stripslashes($line[3])."</p><br /><br />\n\n";
   }
  }
  if($line[7] == "published") {
   echo "<b>$snoc</b><br /><br />\n\n";
   $cfile = file_get_contents("comments/$f.php");
   if($cfile !== "") {
    $cline = explode("\n", $cfile);
    for($e = 0; $e < count($cline) - 1; $e++) {
     $com = explode("\t", $cline[$e]);
     echo "<p class=\"highlight\">".$com[1]."<br /><br />\n\n";
     echo "<b>$lang[61]</b> ";
     if($com[4] == "none") {
	  echo "<b>$com[2]</b> ";
	 }
     else {
	  if(substr($com[4], 0, 7)!=="http://") {
       echo "<a href=\"http://$com[4]\" target=\"_blank\"><b>".$com[2]."</b></a> ";
	  }
	  else {
	   echo "<a href=\"$com[4]\" target=\"_blank\"><b>".$com[2]."</b></a> ";
	  }
     }
     echo " <b>$lang[62] ".date($timeformat, $com[3])."</b></p><br />\n\n";
    }
   }
   else {
    echo "&nbsp;&nbsp;&nbsp;&nbsp;$lang[63]<br /><br />\n\n";
   }
   echo "<br /><b>$lang[64]</b><br /><br />\n\n";
   echo "<form method=\"POST\" action=\"index.php?show=postcomments&p=".$f."\">\n\n";
   echo "<table cellspacing=0 cellpadding=4 border=0 width=\"100%\">\n";
   echo "<tr><td width=\"30%\"> $lang[65] </td>\n";
   echo "<td width=\"70%\"> <input type=\"text\" name=\"vname\" size=20 maxlength=50></td></tr>\n";
   echo "<tr><td width=\"30%\"> $lang[66] </td>\n";
   echo "<td width=\"70%\"> <input type=\"text\" name=\"vemail\" size=20 maxlength=100> <br />(will not be shown) </td></tr>\n";
   echo "<tr><td width=\"30%\"> $lang[67] </td>\n";
   echo "<td width=\"70%\"> <input type=\"text\" name=\"vlocation\" size=20 maxlength=50> <br />($lang[68])</td></tr>\n";
   echo "<tr><td width=\"100%\" colspan=\"2\"> $lang[69] </td></tr>\n";
   session_start();
   $vcode = substr(md5(rand(1, 1000)), 0, 5);
   $_SESSION['verify'] = $vcode;
   echo "<tr><td width=\"30%\"><img src=\"image.php\" name=\"vcode\" /></td>\n";
   echo "<td width=\"70%\">";
   echo "<input type=\"text\" name=\"verify\" size=20 maxlength=50></td></tr>\n";
   echo "<tr><td width=\"30%\" valign=\"top\"> $lang[70] </td>\n";
   echo "<td width=\"70%\"> <textarea cols=25 rows=10 name=\"vcomment\"></textarea></td></tr>\n";
   echo "</table><br />\n";
   echo "<div align=\"center\">\n";
   echo "<button type=\"submit\">$lang[8]</button>&nbsp; \n";
   echo "<button type=\"reset\">$lang[9]</button></div>\n";
   echo "</form>\n\n";
   session_write_close();
  }
 }
}

# Enumerate all posts in a list (archive)
function adisplayall($dir, $count) {
 global $timeformat, $lang;
 $total = $count;
 echo "<h3> $lang[71] </h3>\n";
 echo "<ol type=\"1\">\n";
 for($n=$total; $n > 0; $n--) {
  $data=file_get_contents("$dir/$n.php");
  if($data !== "") {
   $line = explode("\t", $data); 
   $i=count($line); 
   if($line[7] == "published") {
    echo "<li><a href=\"index.php?show=archives&p=".$n."\"><b>" .$line[1]."</b></a>\n";
    echo "<font size=2> $lang[11] ".date($timeformat, $line[2])." </font>\n";
   }
  }
  else { }
 }
 echo "</ol><br />";
}

# Enumerate all posts according to the month and year selected
function adisplaymonthandyear($dir, $count, $month, $year) {
 global $timeformat, $lang;
 $total = $count;
 echo "<h3>".$month." ".$year." $lang[72]</h3>\n";
 echo "<ol type=\"1\">\n";
 for($n=$total; $n > 0; $n--) {
  $data=file_get_contents("$dir/$n.php");
  if($data !== "") {
   $line = explode("\t", $data);
   if(date("F", $line[2])==$month AND date("Y", $line[2])==$year) {
    if($line[7] == "published") {
     echo "<li><a href=\"index.php?show=archives&p=".$n."\"><b>" .$line[1]."</b></a>\n";
     echo "<font size=2> $lang[11] ".date($timeformat, $line[2])." </font>\n";
    }
   }
  }
  else { }
 }
 echo "</ol><br />";
}

# Enumerate all posts according to the category selected
function adisplaycategory($dir, $count, $category) {
 global $timeformat, $lang;
 $total = $count;
 echo "<h3>".tospace(stripslashes($category))."</h3>\n";
 echo "<ol type=\"1\">\n";
 for($n=$total; $n > 0; $n--) {
  $data=file_get_contents("$dir/$n.php");
  if($data !== "") {
   $line = explode("\t", $data);
   if(tospace($category)==$line[6]) {
    if($line[7] == "published") {
     echo "<li><a href=\"index.php?show=archives&p=".$n."\"><b>" .$line[1]."</b></a>\n";
     echo "<font size=2> $lang[11] ".date($timeformat, $line[2])." </font>\n";
    }
   }
  }
  else { }
 }
 echo "</ol><br />";
}

# Enumerate posts to delete or edit exclusive to a user
function editdisplayall($dir, $count, $sessionuser) {
 $total = $count;
 echo "<ol>\n\n";
 for($n=$total; $n > 0; $n--) {
  $data=file_get_contents("$dir/$n.php");
  $line = explode("\t", $data); 
  if(strpos($line[4], $sessionuser) !== FALSE) {
   if($line[1] !== "") {      
    echo "<li><a href=\"admin.php?show=displayedit&n=".$n."\">Edit</a> | \n";
    echo "<a href=\"admin.php?show=displaydelete&n=".$n."\">Delete</a>  <b>".$line[1]." </b>( ".$line[7]." ) </li>\n";
   }
   else {
    echo "<li><a href=\"admin.php?show=displayedit&n=".$n."\">Edit</a> | \n";
    echo "<a href=\"admin.php?show=displaydelete&n=".$n."\">Delete</a>  <b>: No Title :</b>( ".$line[7]." ) </li>\n";
   }
  }
 }
 echo "</ol><br />\n\n";
}

# Enumerate all posts to delete or edit
function editdisplayalladmin($dir, $count) {
 $total = $count;
 echo "<ol>\n\n";
 for($n=$total; $n > 0; $n--) {
  $data=file_get_contents("$dir/$n.php");
  $line = explode("\t", $data); 
  $i=count($line); 
  if($line[1] !== "") {      
   echo "<li><a href=\"admin.php?show=displayedit&n=".$n."\">Edit</a> | \n";
   echo "<a href=\"admin.php?show=displaydelete&n=".$n."\">Delete</a>  <b>".$line[1]." </b>( ".$line[7]." ) </li>\n";
  }
  else {
   echo "<li><a href=\"admin.php?show=displayedit&n=".$n."\">Edit</a> | \n";
   echo "<a href=\"admin.php?show=displaydelete&n=".$n."\">Delete</a>  <b> : No Title : </b>( ".$line[7]." ) </li>\n"; 
  }
 }
 echo "</ol><br />\n\n";
}

# Deletes a file
function deletefile($filenumber, $dir, $c) {
 $count=$c;
 if($filenumber == 1) {
  unlink("$dir/$filenumber.php");
  for($n=2; $n <= $count; $n++) {
   $e = $n - 1;
   rename("$dir/$n.php", "$dir/$e.php");
  }
 }
 if($filenumber > 1) {
  unlink("$dir/$filenumber.php");
  $x = $filenumber + 1;
  for($n=$x; $n <= $count; $n++) {
   $e = $n - 1;
   rename("$dir/$n.php", "$dir/$e.php");
  }
 }
}

# Delete the contents of a file
function deletecontents($filename) {
 $info = "";
 $handle = fopen($filename, "w+");
 fwrite($handle, $info);
}

# Check if there's a user in the session
function check_login() {
 global $blog;
 session_start();
 if(!isset($_SESSION["user"])) {
  header("Location: $blog");
 }
}

# Convert line breaks to HTML line breaks
function newlines($code){
 return stripslashes(str_replace("\r\n", "<br />", $code));
}

# Convert HTML line breaks to normal line breaks for the post editor form
function editor($code) {
 return stripslashes(str_replace("<br />", "\r\n", $code));
}

# Convert whitespaces to dashes (for the categories)
function todash($string) {
 return str_replace(" ", "-", $string);
}

# Convert dashes to whitespaces (for the categories)
function tospace($string) {
 return str_replace("-", " ", $string);
}

# Just a title within the admin panel
function adminpanel($ok) {
 global $lang;
 echo "<h3>$lang[31]</h3>\n";
 echo "<div style=\"margin-left: 20px\"><p>".$ok."</p></div>\n\n";
}

# Another title for the blogviewer
function blogviewer($ok, $blogtitle) {
 echo "<h3>".$blogtitle." </h3>\n";
 echo "<div style=\"margin-left: 20px\"><p>".$ok."</p></div>\n\n";
}

# Delete a theme
function delete_theme( $dir ) {
 if(is_dir($dir)) {
  if($dh = opendir($dir)) {
   while(($file = readdir($dh)) !== false) {
    if($file != "." && $file != "..") {
     if(is_dir( $dir . $file )) {
      delete_theme( $dir . $file . "/" );
	  rmdir( $dir . $file );
	 }
	 else {
      unlink( $dir . $file );
	 }
    }
   }
   closedir($dh);
  }
 }
}

# Compatibility with PHP versions older than 4.3 - I got this from Wordpress - wordpress.org
if(!function_exists('file_get_contents')) {
 function file_get_contents($file) {
  $file = file($file);
  return !$file ? false : implode('', $file);
 }
}

# Check for valid email address - I got this from Dagon Design Form Mailer - dagondesign.com
# Credits: http://www.ilovejackdaniels.com/php/email-address-validation/ 
function ValidEmail($email) {
 if(!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
  return false;  
 }  
 $email_array = explode("@", $email);  
 $local_array = explode(".", $email_array[0]);  
 for($i = 0; $i < sizeof($local_array); $i++) {
  if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {      
   return false;    
  }
 }    
 if(!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
  $domain_array = explode(".", $email_array[1]);    
  if(sizeof($domain_array) < 2) {
   return false; # Not enough parts to domain    
  }
  for($i = 0; $i < sizeof($domain_array); $i++) {
   if(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
    return false;      
   }
  }
 } 
 return true;
}

# Insert the date and time
function date_and_time($timezone, $pattern, $hour, $minute, $second, $month, $day, $year) {

$date_and_time=date($pattern, mktime(date("G")+$timezone+$hour, date("i")+minute, date("s")+$second, date("m")+$month, date("d")+$day, date("Y")+$year));
$date_and_time_raw=mktime(date("G")+$timezone+$hour, date("i")+minute, date("s")+$second, date("m")+$month, date("d")+$day, date("Y")+$year);

 if ($pattern=="") {
  return $date_and_time_raw;
 }
 else {
  return $date_and_time;
 }

// Samples
// echo "Today is ".date_and_time("8", "", "", "", "", "", "", "")." ".date("T")." 8.<br />\n";
// echo "Tomorrow is ".date_and_time("8", "l F j, Y", "", "", "", "", "", "")." ".date("T")." 8.<br />\n";

}

# For the sub-title in the title bar
function subtitle($show, $post, $page) {
 global $tagline, $datadirectory, $lang;
 if($show=="archives" AND $post > 0) {
  if(file_exists("$datadirectory/$post.php")) {
   $postfile = file_get_contents("$datadirectory/$post.php");
   $postline = explode("\n", $postfile);
   $pf = explode("\t", $postline[0]);
   return $pf[1];
  }
  else {
   return $lang[80];
  }
 }
 elseif($show=="pages" AND $page > 0) {
  if(file_exists("pages/$page.php")) {
   $pagefile = file_get_contents("pages/$page.php");
   $pageline = explode("\n", $pagefile);
   $pagef = explode("\t", $pageline[0]);
   return $pagef[1];
  }
  else {
   return $lang[80];
  }
 }
 else {
 switch($show) {
  case "signout":
   return "$lang[76]";
   break;
  case "signin":
   return "$lang[77]";
   break;
  case "archives":
   return "$lang[78]";
   break;
  case "signinok":
   return "$lang[79]";
   break;
   
  # Begin titles for back-end pages
  case "add":
   return "Add A Post";
   break;
  case "post":
   return "Saved Post to Database";
   break;
  case "addpages":
   return "Add A Page";
   break;
  case "postpages":
   return "Saved Page to Database";
   break;
  case "addcategories":
   return "Add A Category";
   break;
  case "postcategories":
   return "Saved Category to Database";
   break;
  case "editjournals":
   return "Edit Or Delete A Post";
   break;
  case "displayedit":
   return "Post Editor";
   break;
  case "goedit":
   return "Post Edited";
   break;
  case "editpages":
   return "Edit Or Delete A Page";
   break;
  case "displayeditpages":
   return "Page Editor";
   break;
  case "goeditpages":
   return "Page Edited";
   break;
  case "editcategories":
   return "Edit Or Delete A Category";
   break;
  case "displayeditcategories":
   return "Category Editor";
   break;
  case "goeditcategories":
   return "Category Edited";
   break;
  case "editarchives":
   return "Delete Archives";
   break;
  case "editconfig":
   return "Configuration Settings";
   break;
  case "goconfig":
   return "Configuration Settings Saved";
   break;
  case "editdeletecomments":
   return "Edit Or Delete A Comment";
   break;
  case "editcomments":
   return "Comment Editor";
   break;
  case "gocomments":
   return "Comment Edited";
   break;
  case "editads":
   return "Add-on Editor";
   break;
  case "goads":
   return "Add-on Edited";
   break;
  case "themes":
   return "Choose A Theme";
   break;
  case "gotheme":
   return "Theme Selected";
   break;
  case "theme_editor":
   return "Theme Editor";
   break;
  case "go_theme_editor":
   return "Theme Edited";
   break;
  case "new_theme":
   return "Create A New Theme";
   break;
  case "go_new_theme":
   return "New Theme Created";
   break;
  case "delete_theme":
   return "Theme Deleted";
   break;
  case "displaydelete":
   return "Post Deleted";
   break;
  case "deletecomments":
   return "Comment Deleted";
   break;
  case "displaydeletecategories":
   return "Category Deleted";
   break;
  case "deleteads":
   return "Add-ons Deleted";
   break;
  case "deletearchives":
   return "Archives Deleted";
   break;
  case "refreshhits":
   return "Hits Refreshed";
   break;
  case "statistics":
   return "Blog Site Statistics";
   break;
  case "userconfig":
   return "User Configuration Settings";
   break;
  case "addposter":
   return "User Information Added to Database";
   break;
  case "edituser":
   return "User Information Edited";
   break;
  case "deleteuser":
   return "Deleted User Information";
   break;
  default:
   return $tagline;
   break;
 }
 }
}

# SuperFilter! Hee hee hee
function super_filter($data) {
 return  htmlentities(strip_tags(utf8_decode(str_replace("\\", "", str_replace("/", "", str_replace(".", "", $data))))));
}

# Just letters, numbers, dashes, single quotes, underscores and spaces
function string_filter($data) {
 return preg_replace('/[^a-z 0-9 - \' _]/i', '', $data);
}

# Just letters, numbers, dashes and underscores - no spaces
function string_filter_nospace($data) {
 return preg_replace('/[^a-z 0-9 - _]/i', '', str_replace(" ", "", $data));
}

# Just letters, numbers, dashes, underscores, periods and @ signs - no spaces
function string_filter_email($data) {
 return preg_replace('/[^a-z 0-9 - _ . @]/i', '', str_replace(" ", "", $data));
}

# Just letters, numbers, dashes, underscores, periods, slashes and  colons - no spaces
function string_filter_url($data) {
 return preg_replace('/[^a-z 0-9 - _ . \/ :]/i', '', str_replace(" ", "", $data));
}

# SuperFilter! Hee hee hee
function string_filter_comment($data) {
 return  str_replace("\r\n", "<br />", htmlentities(strip_tags(utf8_decode(str_replace("\\", "", $data))), ENT_QUOTES));
}

# For filtering integers: Just numbers
function int_filter($data) {
 return preg_replace('/[^0-9]/i', '', str_replace(" ", "", $data));
}

# Generate RSS stuff
function generaterss() {
 global $blogtitle, $blog, $tagline, $datadirectory, $total, $timeformat;
 $id = $total;
 $d = $id - 5;
 header("Content-type: text/xml");
 echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
 echo "<rss version=\"2.0\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\">\n";
 echo "<channel>\n";
 echo "<title> $blogtitle </title>\n";
 echo "<link> $blog </link>\n";
 echo "<description> $tagline </description>\n";
 for($rss = $id; $rss > $d; $rss--) {
  if(file_exists("$datadirectory/$rss.php")) {
   $rssfile = file_get_contents("$datadirectory/$rss.php");
   $whatrss = explode("\t", $rssfile);
   static $regex = array(
    '/<br />/s' => ' ',
    '/<(.*?)>/s' => ''
   );
   $wri = preg_replace(array_keys($regex), array_values($regex), stripslashes($whatrss[3]));        
   $wri2 = str_replace("&nbsp;", " ", $wri);
   $wri3 = str_replace("&", "&amp;", $wri2);
   echo "<item>\n";
   echo "<title>$whatrss[1]</title>\n";
   echo "<link>$blog/index.php?show=archives&amp;p=$rss</link>\n";
   echo "<description>$wri2</description>\n";
   echo "<dc:creator>$whatrss[4]</dc:creator>\n";
   echo "<dc:date>".date($timeformat, $whatrss[2])."</dc:date>\n";
   echo "</item>\n";
  }
 }
 echo "</channel>\n";
 echo "</rss>";
}
		
?>